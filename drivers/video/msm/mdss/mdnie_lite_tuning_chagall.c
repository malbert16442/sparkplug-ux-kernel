/* Copyright (c) 2009-2011, Code Aurora Forum. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/clk.h>
#include <linux/mutex.h>
#include <linux/poll.h>
#include <linux/wait.h>
#include <linux/fs.h>
#include <linux/irq.h>
#include <linux/mm.h>
#include <linux/fb.h>
#include <linux/ctype.h>
#include <linux/miscdevice.h>
#include <linux/dma-mapping.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/fb.h>
#include <linux/msm_mdp.h>
#include <linux/ioctl.h>
#include <linux/lcd.h>

#include "mdss_fb.h"
#include "mdss_panel.h"
#include "mdss_dsi.h"
#include "mdnie_lite_tuning_chagall.h"
#include "mdnie_lite_tuning_data_chagall.h"

#if defined(CONFIG_TDMB)
#include "mdnie_lite_tuning_data_dmb.h"
#endif

int play_speed_1_5;
static struct mipi_samsung_driver_data *mdnie_msd;

//#define MDNIE_LITE_TUN_DATA_DEBUG
#define DPRINT(x...)	printk(KERN_ERR "[mdnie lite] " x)


#define PAYLOAD1 mdni_tune_cmd[1]
#define PAYLOAD2 mdni_tune_cmd[3]

#define INPUT_PAYLOAD1(x) PAYLOAD1.payload = x
#define INPUT_PAYLOAD2(x) PAYLOAD2.payload = x

static char mdnie_enable_1[] = {0xB0, 0x45};
static char mdnie_enable_2[] = {0xB6, 0x03};
static char mdnie_global_1[] = {MDNIE_GLOBAL_COMMAND, MDNIE_GLOBAL_FIRST};
static char mdnie_data_1[MDNIE_TUNE_FIRST_SIZE] = {0,};
static char mdnie_global_2[] = {MDNIE_GLOBAL_COMMAND, MDNIE_GLOBAL_SECOND};
static char mdnie_data_2[MDNIE_TUNE_SECOND_SIZE] = {0,};
static char white_rgb_buf[MDNIE_TUNE_FIRST_SIZE] = {0,};


#if 0
static char tune_data1_adb[MDNIE_TUNE_FIRST_SIZE] = {0,};
static char tune_data2_adb[MDNIE_TUNE_SECOND_SIZE] = {0,};

void copy_tuning_data_from_adb(char *data1, char *data2)
{
	memcpy(tune_data1_adb, data1, MDNIE_TUNE_FIRST_SIZE);
	memcpy(tune_data2_adb, data2, MDNIE_TUNE_SECOND_SIZE);
}
#endif

static struct dsi_cmd_desc mdni_enable_cmd[] = {
	{{DTYPE_DCS_LWRITE, 1, 0, 0, 0,	sizeof(mdnie_enable_1)}, mdnie_enable_1},
	{{DTYPE_DCS_LWRITE, 1, 0, 0, 0,	sizeof(mdnie_enable_2)}, mdnie_enable_2},
};

static struct dsi_cmd_desc mdni_tune_cmd[] = {
	{{DTYPE_DCS_LWRITE, 1, 0, 0, 0,	sizeof(mdnie_global_1)}, mdnie_global_1},
	{{DTYPE_DCS_LWRITE, 1, 0, 0, 17, sizeof(mdnie_data_1)}, mdnie_data_1},
	{{DTYPE_DCS_LWRITE, 1, 0, 0, 0,	sizeof(mdnie_global_2)}, mdnie_global_2},
	{{DTYPE_DCS_LWRITE, 1, 0, 0, 17, sizeof(mdnie_data_2)}, mdnie_data_2},
};

#if 0
extern int mdnie_adb_test;
#endif

struct mdnie_lite_tun_type mdnie_tun_state = {
	.mdnie_enable = false,
	.scenario = mDNIe_UI_MODE,
	.background = STANDARD_MODE,
	.outdoor = OUTDOOR_OFF_MODE,
	.accessibility = ACCESSIBILITY_OFF,
#if defined(CONFIG_TDMB)
	.dmb = DMB_MODE_OFF,
#endif
	.scr_white_red = 0xff,
	.scr_white_green = 0xff,
	.scr_white_blue = 0xff,
};

#if !defined(CONFIG_TDMB)
const
#endif
char scenario_name[MAX_mDNIe_MODE][16] = {
	"UI_MODE",
	"VIDEO_MODE",
	"VIDEO_WARM_MODE",
	"VIDEO_COLD_MODE",
	"CAMERA_MODE",
	"NAVI",
	"GALLERY_MODE",
	"VT_MODE",
	"BROWSER",
	"eBOOK",
	"EMAIL",
};

const char background_name[MAX_BACKGROUND_MODE][10] = {
	"DYNAMIC",
	"STANDARD",
	"NATURAL",
	"MOVIE",
	"AUTO",
};

const char outdoor_name[MAX_OUTDOOR_MODE][20] = {
	"OUTDOOR_OFF_MODE",
	"OUTDOOR_ON_MODE",
};

const char accessibility_name[ACCESSIBILITY_MAX][20] = {
	"ACCESSIBILITY_OFF",
	"NEGATIVE_MODE",
	"COLOR_BLIND_MODE",
	"SCREEN_CURTAIN_MODE",
};

char mdnie_app_name[][NAME_STRING_MAX] = {
	"UI_APP",
	"VIDEO_APP",
	"VIDEO_WARM_APP",
	"VIDEO_COLD_APP",
	"CAMERA_APP",night
	"NAVI_APP",
	"GALLERY_APP",
	"VT_APP",
	"BROWSER_APP",
	"eBOOK_APP",
	"EMAIL_APP",
	"TDMB_APP",
};

char mdnie_mode_name[][NAME_STRING_MAX] = {
	"DYNAMIC_MODE",
	"STANDARD_MODE",
#if defined(NATURAL_MODE_ENABLE)
	"NATURAL_MODE",
#endif
	"MOVIE_MODE",
	"AUTO_MODE",
	"READING_MODE",
};

char outdoor_name[][NAME_STRING_MAX] = {
	"OUTDOOR_OFF_MODE",
	"OUTDOOR_ON_MODE",
};

void send_dsi_tcon_mdnie_register(struct samsung_display_driver_data *vdd,
	struct dsi_cmd_desc *tune_data_dsi0, struct dsi_cmd_desc *tune_data_dsi1, struct mdnie_lite_tun_type *mdnie_tune_state)
{
	if (vdd == NULL || !vdd->support_mdnie_lite)
		return;

	/* DUAL PANEL CHECK */
	if (!IS_ERR_OR_NULL(tune_data_dsi0) && !IS_ERR_OR_NULL(tune_data_dsi1)) {
		if (vdd->support_hall_ic) {
			if (tune_data_dsi0 && tune_data_dsi1 && mdnie_tune_state) {
				/* foder open : 0(primary panel), close : 1(secondary panel)*/
				if (!vdd->display_status_dsi[DSI_CTRL_0].hall_ic_status) {
					/* primary(internal) panel */
					vdd->mdnie_tune_data[DSI_CTRL_0].mdnie_tune_packet_tx_cmds_dsi.cmds = tune_data_dsi0;
					vdd->mdnie_tune_data[DSI_CTRL_0].mdnie_tune_packet_tx_cmds_dsi.cmd_cnt = mdnie_data.dsi0_bypass_mdnie_size;
				} else {
					/* secondary(external) panel */
					vdd->mdnie_tune_data[DSI_CTRL_1].mdnie_tune_packet_tx_cmds_dsi.cmds = tune_data_dsi1;
					vdd->mdnie_tune_data[DSI_CTRL_1].mdnie_tune_packet_tx_cmds_dsi.cmd_cnt = mdnie_data.dsi1_bypass_mdnie_size;
				}

				DPRINT("DUAL index : %d hbm : %d mdnie_bypass : %d mdnie_accessibility : %d  mdnie_app: %d mdnie_mode : %d\n",
					vdd->display_status_dsi[DSI_CTRL_0].hall_ic_status, mdnie_tune_state->hbm_enable, mdnie_tune_state->mdnie_bypass, mdnie_tune_state->mdnie_accessibility,
					mdnie_tune_state->mdnie_app, mdnie_tune_state->mdnie_mode);

				mdss_samsung_send_cmd(vdd->ctrl_dsi[DSI_CTRL_0], PANEL_MDNIE_TUNE);
			} else
				DPRINT("DUAL Command Tx Fail, tune_data_dsi0=%p, tune_data_dsi1=%p,vdd=%p, mdnie_tune_state=%p\n",
					tune_data_dsi0, tune_data_dsi0, vdd, mdnie_tune_state);
		} else {
			if (tune_data_dsi0 && tune_data_dsi1 && mdnie_tune_state) {
				vdd->mdnie_tune_data[0].mdnie_tune_packet_tx_cmds_dsi.cmds = tune_data_dsi0;
				vdd->mdnie_tune_data[0].mdnie_tune_packet_tx_cmds_dsi.cmd_cnt = mdnie_data.dsi0_bypass_mdnie_size;

				vdd->mdnie_tune_data[1].mdnie_tune_packet_tx_cmds_dsi.cmds = tune_data_dsi1;
				vdd->mdnie_tune_data[1].mdnie_tune_packet_tx_cmds_dsi.cmd_cnt = mdnie_data.dsi1_bypass_mdnie_size;

				/* TODO: Tx command */
				DPRINT("DUAL Command Tx Fail(TODO  DUAL PANEL), tune_data_dsi0=%p, tune_data_dsi1=%p,vdd=%p, mdnie_tune_state=%p\n",
					tune_data_dsi0, tune_data_dsi0, vdd, mdnie_tune_state);
			} else
				DPRINT("DUAL Command Tx Fail, tune_data_dsi0=%p, tune_data_dsi1=%p,vdd=%p, mdnie_tune_state=%p\n",
					tune_data_dsi0, tune_data_dsi0, vdd, mdnie_tune_state);
		}
	} else {
		if (tune_data_dsi0 && mdnie_tune_state) {
			DPRINT("SINGLE index : %d hbm : %d mdnie_bypass : %d mdnie_accessibility : %d  mdnie_app: %d mdnie_mode : %d\n",
				mdnie_tune_state->index, mdnie_tune_state->hbm_enable, mdnie_tune_state->mdnie_bypass, mdnie_tune_state->mdnie_accessibility,
				mdnie_tune_state->mdnie_app, mdnie_tune_state->mdnie_mode);

			if (vdd->ctrl_dsi[DSI_CTRL_0]->cmd_sync_wait_broadcast) { /* Dual DSI */
				vdd->mdnie_tune_data[DSI_CTRL_1].mdnie_tune_packet_tx_cmds_dsi.cmds = tune_data_dsi0;
				vdd->mdnie_tune_data[DSI_CTRL_1].mdnie_tune_packet_tx_cmds_dsi.cmd_cnt = mdnie_data.dsi0_bypass_mdnie_size;
				mdss_samsung_send_cmd(vdd->ctrl_dsi[DSI_CTRL_1], PANEL_MDNIE_TUNE);
			} else { /* Single DSI */
				vdd->mdnie_tune_data[DSI_CTRL_0].mdnie_tune_packet_tx_cmds_dsi.cmds = tune_data_dsi0;
				vdd->mdnie_tune_data[DSI_CTRL_0].mdnie_tune_packet_tx_cmds_dsi.cmd_cnt = mdnie_data.dsi0_bypass_mdnie_size;
				mdss_samsung_send_cmd(vdd->ctrl_dsi[DSI_CTRL_0], PANEL_MDNIE_TUNE);
			}
		} else
			DPRINT("SINGLE Command Tx Fail, tune_data_dsi0=%p, vdd=%p, mdnie_tune_state=%p \n", tune_data_dsi0, vdd, mdnie_tune_state);
	}
}

int update_dsi_tcon_mdnie_register(struct samsung_display_driver_data *vdd)
{
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;
	struct mdnie_lite_tun_type *real_mdnie_tune_state = NULL;
	struct dsi_cmd_desc *tune_data_dsi0 = NULL;
	struct dsi_cmd_desc *tune_data_dsi1 = NULL;

	if (vdd == NULL || !vdd->support_mdnie_lite)
		return 0;

	if (vdd->mfd_dsi[DISPLAY_1]->panel_info->cont_splash_enabled ||
		vdd->mfd_dsi[DISPLAY_1]->panel_info->blank_state != MDSS_PANEL_BLANK_UNBLANK) {
		pr_err("%s : do not send mdnie data (%d) (%d)\n", __func__ ,
			vdd->mfd_dsi[DISPLAY_1]->panel_info->cont_splash_enabled,
			vdd->mfd_dsi[DISPLAY_1]->panel_info->blank_state);
		return 0;
	}

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		real_mdnie_tune_state = mdnie_tune_state;
		/*
		*	Checking HBM mode first.
		*/
		if (mdnie_tune_state->vdd->lux >= mdnie_tune_state->vdd->enter_hbm_lux)
			mdnie_tune_state->hbm_enable = true;
		else
			mdnie_tune_state->hbm_enable = false;

		/*
		* mDnie priority
		* Accessibility > HBM > Screen Mode
		*/
		if (mdnie_tune_state->mdnie_bypass == BYPASS_ENABLE) {
			if (mdnie_tune_state->index == DSI_CTRL_0)
				tune_data_dsi0 = mdnie_data.DSI0_BYPASS_MDNIE;
			else
				tune_data_dsi1 = mdnie_data.DSI1_BYPASS_MDNIE;
		} else if (mdnie_tune_state->mdnie_accessibility == COLOR_BLIND) {
			if (mdnie_tune_state->index == DSI_CTRL_0)
				tune_data_dsi0  = mdnie_data.DSI0_COLOR_BLIND_MDNIE;
			else
				tune_data_dsi1  = mdnie_data.DSI1_COLOR_BLIND_MDNIE;
		} else if (mdnie_tune_state->mdnie_accessibility == NEGATIVE) {
			if (mdnie_tune_state->index == DSI_CTRL_0)
				tune_data_dsi0  = mdnie_data.DSI0_NEGATIVE_MDNIE;
			else
				tune_data_dsi1  = mdnie_data.DSI1_NEGATIVE_MDNIE;
		} else if (mdnie_tune_state->mdnie_accessibility == CURTAIN) {
			if (mdnie_tune_state->index == DSI_CTRL_0)
				tune_data_dsi0  = mdnie_data.DSI0_CURTAIN;
			else
				tune_data_dsi1  = mdnie_data.DSI1_CURTAIN;
		} else if (mdnie_tune_state->mdnie_accessibility == GRAYSCALE) {
			if (mdnie_tune_state->index == DSI_CTRL_0)
				tune_data_dsi0  = mdnie_data.DSI0_GRAYSCALE_MDNIE;
			else
				tune_data_dsi1  = mdnie_data.DSI1_GRAYSCALE_MDNIE;
		} else if (mdnie_tune_state->mdnie_accessibility == GRAYSCALE_NEGATIVE) {
			if (mdnie_tune_state->index == DSI_CTRL_0)
				tune_data_dsi0  = mdnie_data.DSI0_GRAYSCALE_NEGATIVE_MDNIE;
			else
				tune_data_dsi1  = mdnie_data.DSI1_GRAYSCALE_NEGATIVE_MDNIE;
		} else if (mdnie_tune_state->hbm_enable == true) {
			if (vdd->dtsi_data[mdnie_tune_state->index].hbm_ce_text_mode_support &&
				((mdnie_tune_state->mdnie_app == BROWSER_APP) || (mdnie_tune_state->mdnie_app == eBOOK_APP)))
					if (mdnie_tune_state->index == DSI_CTRL_0)
						tune_data_dsi0  = mdnie_data.DSI0_HBM_CE_TEXT_MDNIE;
					else
						tune_data_dsi1  = mdnie_data.DSI1_HBM_CE_TEXT_MDNIE;
			else {
				if (mdnie_tune_state->index == DSI_CTRL_0)
					tune_data_dsi0  = mdnie_data.DSI0_HBM_CE_MDNIE;
				else
					tune_data_dsi1  = mdnie_data.DSI1_HBM_CE_MDNIE;
			}
		} else if (mdnie_tune_state->night_mode_enable == true) {
				if (mdnie_tune_state->index == DSI_CTRL_0)
					tune_data_dsi0  = mdnie_data.DSI0_NIGHT_MODE_MDNIE;
				else
					tune_data_dsi1  = mdnie_data.DSI1_NIGHT_MODE_MDNIE;
		} else if (mdnie_tune_state->hmt_color_temperature) {
			if (mdnie_tune_state->index == DSI_CTRL_0)
				tune_data_dsi0 = mdnie_data.hmt_color_temperature_tune_value_dsi0[mdnie_tune_state->hmt_color_temperature];
			else
				tune_data_dsi1 = mdnie_data.hmt_color_temperature_tune_value_dsi1[mdnie_tune_state->hmt_color_temperature];
		} else if (mdnie_tune_state->mdnie_app == EMAIL_APP) {
			/*
				Some kind of panel doesn't suooprt EMAIL_APP mode, but SSRM module use same control logic.
				It means SSRM doesn't consider panel unique character.
				To support this issue eBOOK_APP used insted of EMAIL_APP under EMAIL_APP doesn't exist status..
			*/
			if (mdnie_tune_state->index == DSI_CTRL_0) {
				tune_data_dsi0 = mdnie_data.mdnie_tune_value_dsi0[mdnie_tune_state->mdnie_app][mdnie_tune_state->mdnie_mode][mdnie_tune_state->outdoor];

				if (!tune_data_dsi0)
					tune_data_dsi0 = mdnie_data.mdnie_tune_value_dsi0[eBOOK_APP][mdnie_tune_state->mdnie_mode][mdnie_tune_state->outdoor];
			} else {
				tune_data_dsi1 = mdnie_data.mdnie_tune_value_dsi1[mdnie_tune_state->mdnie_app][mdnie_tune_state->mdnie_mode][mdnie_tune_state->outdoor];

				if (!tune_data_dsi1)
					tune_data_dsi1 = mdnie_data.mdnie_tune_value_dsi1[eBOOK_APP][mdnie_tune_state->mdnie_mode][mdnie_tune_state->outdoor];
			}
		} else {
			if (mdnie_tune_state->index == DSI_CTRL_0)
				tune_data_dsi0 = mdnie_data.mdnie_tune_value_dsi0[mdnie_tune_state->mdnie_app][mdnie_tune_state->mdnie_mode][mdnie_tune_state->outdoor];
			else
				tune_data_dsi1 = mdnie_data.mdnie_tune_value_dsi1[mdnie_tune_state->mdnie_app][mdnie_tune_state->mdnie_mode][mdnie_tune_state->outdoor];
		}

		if (!tune_data_dsi0 && (mdnie_tune_state->index == DSI_CTRL_0)) {
			DPRINT("%s index : %d tune_data is NULL hbm : %d mdnie_bypass : %d mdnie_accessibility : %d  mdnie_app: %d mdnie_mode : %d\n", __func__,
				mdnie_tune_state->index, mdnie_tune_state->hbm_enable, mdnie_tune_state->mdnie_bypass, mdnie_tune_state->mdnie_accessibility,
				mdnie_tune_state->mdnie_app, mdnie_tune_state->mdnie_mode);
			return -EFAULT;
		} else if (!tune_data_dsi1 && (mdnie_tune_state->index == DSI_CTRL_1)) {
			DPRINT("%s index : %d tune_data is NULL hbm : %d mdnie_bypass : %d mdnie_accessibility : %d  mdnie_app: %d mdnie_mode : %d\n", __func__,
				mdnie_tune_state->index, mdnie_tune_state->hbm_enable, mdnie_tune_state->mdnie_bypass, mdnie_tune_state->mdnie_accessibility,
				mdnie_tune_state->mdnie_app, mdnie_tune_state->mdnie_mode);
			return -EFAULT;
		} else if (likely(tune_data_dsi0)) {
			if(mdnie_data.dsi0_rgb_sensor_mdnie_index == 1) {
				mdnie_tune_state->scr_white_red = tune_data_dsi0[mdnie_data.mdnie_step_index[MDNIE_STEP1]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]];
				mdnie_tune_state->scr_white_green = tune_data_dsi0[mdnie_data.mdnie_step_index[MDNIE_STEP1]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]];
				mdnie_tune_state->scr_white_blue = tune_data_dsi0[mdnie_data.mdnie_step_index[MDNIE_STEP1]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]];
			} else if(mdnie_data.dsi0_rgb_sensor_mdnie_index == 2) {
				mdnie_tune_state->scr_white_red = tune_data_dsi0[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]];
				mdnie_tune_state->scr_white_green = tune_data_dsi0[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]];
				mdnie_tune_state->scr_white_blue = tune_data_dsi0[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]];
			}
		} else if (likely(tune_data_dsi1)) {
			if(mdnie_data.dsi1_rgb_sensor_mdnie_index == 1) {
				mdnie_tune_state->scr_white_red = tune_data_dsi1[mdnie_data.mdnie_step_index[MDNIE_STEP1]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]];
				mdnie_tune_state->scr_white_green = tune_data_dsi1[mdnie_data.mdnie_step_index[MDNIE_STEP1]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]];
				mdnie_tune_state->scr_white_blue = tune_data_dsi1[mdnie_data.mdnie_step_index[MDNIE_STEP1]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]];
			} else if(mdnie_data.dsi1_rgb_sensor_mdnie_index == 2) {
				mdnie_tune_state->scr_white_red = tune_data_dsi1[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]];
				mdnie_tune_state->scr_white_green = tune_data_dsi1[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]];
				mdnie_tune_state->scr_white_blue = tune_data_dsi1[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]];
			}
		}
	}

	send_dsi_tcon_mdnie_register(vdd, tune_data_dsi0, tune_data_dsi1, real_mdnie_tune_state);

	return 0;
}

static ssize_t night_mode_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int buffer_pos = 0;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		buffer_pos += snprintf(buf, 256, "%d %d", mdnie_tune_state->night_mode_enable, mdnie_tune_state->night_mode_index);
	}
	return buffer_pos;
}

static ssize_t night_mode_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t size)
{
	int enable, idx;
	char *buffer;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;
	struct mdnie_lite_tun_type *real_mdnie_tune_state = NULL;
	struct samsung_display_driver_data *vdd = NULL;

	sscanf(buf, "%d %d", &enable, &idx);

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		real_mdnie_tune_state = mdnie_tune_state;

		if (!vdd)
			vdd = mdnie_tune_state->vdd;

		mdnie_tune_state->night_mode_enable = enable;
		if (mdnie_tune_state->index == DSI_CTRL_0) {
			if(((idx >=0) && (idx < mdnie_data.dsi0_max_night_mode_index)) && (enable == true)) {
				if (!IS_ERR_OR_NULL(mdnie_data.dsi0_night_mode_table)) {
					buffer = &mdnie_data.dsi0_night_mode_table[(MDNIE_NIGHT_MODE_CMD_SIZE * idx)];
					if (!IS_ERR_OR_NULL(mdnie_data.DSI0_NIGHT_MODE_MDNIE_SCR)) {
						memcpy(&mdnie_data.DSI0_NIGHT_MODE_MDNIE_SCR[mdnie_data.mdnie_color_blinde_cmd_offset],
							buffer, MDNIE_NIGHT_MODE_CMD_SIZE);
						mdnie_tune_state->night_mode_index = idx;
					}
				}
			}
		} else {
			if(((idx >=0) && (idx < mdnie_data.dsi1_max_night_mode_index)) && (enable == true)) {
				if (!IS_ERR_OR_NULL(mdnie_data.dsi1_night_mode_table)) {
					buffer = &mdnie_data.dsi1_night_mode_table[(MDNIE_NIGHT_MODE_CMD_SIZE * idx)];
					if (!IS_ERR_OR_NULL(mdnie_data.DSI1_NIGHT_MODE_MDNIE_SCR)) {
						memcpy(&mdnie_data.DSI1_NIGHT_MODE_MDNIE_SCR[mdnie_data.mdnie_color_blinde_cmd_offset],
							buffer, MDNIE_NIGHT_MODE_CMD_SIZE);
						mdnie_tune_state->night_mode_index = idx;
					}
				}
			}
		}
	}

	update_dsi_tcon_mdnie_register(vdd);
	return size;
}

static ssize_t mdnie_ldu_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int buffer_pos = 0;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		buffer_pos += snprintf(buf, 256, "%d %d %d ", mdnie_tune_state->scr_white_red, mdnie_tune_state->scr_white_green, mdnie_tune_state->scr_white_blue);
	}
	return buffer_pos;
}

static ssize_t mdnie_ldu_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t size)
{
	int i, j, idx;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;
	struct mdnie_lite_tun_type *real_mdnie_tune_state = NULL;
	struct dsi_cmd_desc *ldu_tunning_data = NULL;
	struct samsung_display_driver_data *vdd = NULL;

	sscanf(buf, "%d", &idx);

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		real_mdnie_tune_state = mdnie_tune_state;

		if (!vdd)
			vdd = mdnie_tune_state->vdd;

		if (mdnie_tune_state->index == DSI_CTRL_0) {
			if((idx >=0) && (idx < mdnie_data.dsi0_max_adjust_ldu)) {
				for (i = 0; i < MAX_APP_MODE; i++) {
					for (j = 0; j < MAX_MODE; j++) {
						if ((mdnie_data.mdnie_tune_value_dsi0[i][j][0] != NULL) && (i != eBOOK_APP) && (j != READING_MODE)) {
							if(mdnie_data.dsi0_adjust_ldu_table[j] != NULL) {
								ldu_tunning_data = mdnie_data.mdnie_tune_value_dsi0[i][j][0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 1];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 2];
							}
						}
					}
				}
			}
		} else {
			if((idx >=0) && (idx < mdnie_data.dsi1_max_adjust_ldu)) {
				for (i = 0; i < MAX_APP_MODE; i++) {
					for (j = 0; j < MAX_MODE; j++) {
						if ((mdnie_data.mdnie_tune_value_dsi1[i][j][0] != NULL) && (i != eBOOK_APP) && (j != READING_MODE)) {
							if(mdnie_data.dsi1_adjust_ldu_table[j] != NULL) {
								ldu_tunning_data = mdnie_data.mdnie_tune_value_dsi1[i][j][0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 1];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 2];
							}
						}
					}
				}
			}
		}
	}

	update_dsi_tcon_mdnie_register(vdd);
	return size;
}

static ssize_t hmt_color_temperature_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		DPRINT("Current color temperature : %d\n", mdnie_tune_state->hmt_color_temperature);
	}

	return snprintf(buf, 256, "Current color temperature : %d\n", mdnie_tune_state->hmt_color_temperature);
}

static ssize_t hmt_color_temperature_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t size)
{
	int value;
	int backup;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;
	struct samsung_display_driver_data *vdd = NULL;

	sscanf(buf, "%d", &value);

	if (value < HMT_COLOR_TEMP_OFF || value >= HMT_COLOR_TEMP_MAX) {
		DPRINT("[ERROR] wrong color temperature value : %d\n", value);
		return size;
	}

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		if (mdnie_tune_state->mdnie_accessibility == NEGATIVE) {
			DPRINT("already negative mode(%d), do not update color temperature(%d)\n",
				mdnie_tune_state->mdnie_accessibility, value);
			return size;
		}

		if (!vdd)
			vdd = mdnie_tune_state->vdd;

		backup = mdnie_tune_state->hmt_color_temperature;
		mdnie_tune_state->hmt_color_temperature = value;

		DPRINT("%s : (%d) -> (%d)\n", __func__, backup, value);
	}

	update_dsi_tcon_mdnie_register(vdd);

	return size;
}

static DEVICE_ATTR(mode, 0664, mode_show, mode_store);
static DEVICE_ATTR(scenario, 0664, scenario_show, scenario_store);
static DEVICE_ATTR(outdostatic ssize_t mdnie_ldu_show(struct device *dev,
		struct devistatic ssize_t mdnie_ldu_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int buffer_pos = 0;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		buffer_pos += snprintf(buf, 256, "%d %d %d ", mdnie_tune_state->scr_white_red, mdnie_tune_state->scr_white_green, mdnie_tune_state->scr_white_blue);
	}
	return buffer_pos;
}

static ssize_t mdnie_ldu_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t size)
{
	int i, j, idx;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;
	struct mdnie_lite_tun_type *real_mdnie_tune_state = NULL;
	struct dsi_cmd_desc *ldu_tunning_data = NULL;
	struct samsung_display_driver_data *vdd = NULL;

	sscanf(buf, "%d", &idx);

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		real_mdnie_tune_state = mdnie_tune_state;

		if (!vdd)
			vdd = mdnie_tune_state->vdd;

		if (mdnie_tune_state->index == DSI_CTRL_0) {
			if((idx >=0) && (idx < mdnie_data.dsi0_max_adjust_ldu)) {
				for (i = 0; i < MAX_APP_MODE; i++) {
					for (j = 0; j < MAX_MODE; j++) {
						if ((mdnie_data.mdnie_tune_value_dsi0[i][j][0] != NULL) && (i != eBOOK_APP) && (j != READING_MODE)) {
							if(mdnie_data.dsi0_adjust_ldu_table[j] != NULL) {
								ldu_tunning_data = mdnie_data.mdnie_tune_value_dsi0[i][j][0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 1];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 2];
							}
						}
					}
				}
			}
		} else {
			if((idx >=0) && (idx < mdnie_data.dsi1_max_adjust_ldu)) {
				for (i = 0; i < MAX_APP_MODE; i++) {
					for (j = 0; j < MAX_MODE; j++) {
						if ((mdnie_data.mdnie_tune_value_dsi1[i][j][0] != NULL) && (i != eBOOK_APP) && (j != READING_MODE)) {
							if(mdnie_data.dsi1_adjust_ldu_table[j] != NULL) {
								ldu_tunning_data = mdnie_data.mdnie_tune_value_dsi1[i][j][0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 1];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 2];
						static ssize_t mdnie_ldu_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t size)
{
	int i, j, idx;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;
	struct mdnie_lite_tun_type *real_mdnie_tune_state = NULL;
	struct dsi_cmd_desc *ldu_tunning_data = NULL;
	struct samsung_display_driver_data *vdd = NULL;

	sscanf(buf, "%d", &idx);

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		real_mdnie_tune_state = mdnie_tune_state;

		if (!vdd)
			vdd = mdnie_tune_state->vdd;

		if (mdnie_tune_state->index == DSI_CTRL_0) {
			if((idx >=0) && (idx < mdnie_data.dsi0_max_adjust_ldu)) {
				for (i = 0; i < MAX_APP_MODE; i++) {
					for (j = 0; j < MAX_MODE; j++) {
						if ((mdnie_data.mdnie_tune_value_dsi0[i][j][0] != NULL) && (i != eBOOK_APP) && (j != READING_MODE)) {
							if(mdnie_data.dsi0_adjust_ldu_table[j] != NULL) {
								ldu_tunning_data = mdnie_data.mdnie_tune_value_dsi0[i][j][0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 1];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 2];
							}
						}
					}
				}
			}
		} else {
			if((idx >=0) && (idx < mdnie_data.dsi1_max_adjust_ldu)) {
				for (i = 0; i < MAX_APP_MODE; i++) {
					for (j = 0; j < MAX_MODE; j++) {
						if ((mdnie_data.mdnie_tune_value_dsi1[i][j][0] != NULL) && (i != eBOOK_APP) && (j != READING_MODE)) {
							if(mdnie_data.dsi1_adjust_ldu_table[j] != NULL) {
								ldu_tunning_data = mdnie_data.mdnie_tune_value_dsi1[i][j][0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].p	}
						}
					}
				}
			}
		}
	}

	update_dsi_tcon_mdnie_register(vdd);
	return size;
}

static ssize_t hmt_color_temperature_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		DPRINT("Current color temperature : %d\n", mdnie_tune_state->hmt_color_temperature);
	}

	return snprintf(buf, 256, "Current color temperature : %d\n", mdnie_tune_state->hmt_color_temperature);
}

static ssize_t hmt_color_temperature_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t size)
{
	int value;
	int backup;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;
	struct samsung_display_driver_data *vdd = NULL;

	sscanf(buf, "%d", &value);

	if (value < HMT_COLOR_TEMP_OFF || value >= HMT_COLOR_TEMP_MAX) {
		DPRINT("[ERROR] wrong color temperature value : %d\n", value);
		return size;
	}

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		if (mdnie_tune_state->mdnie_accessibility == NEGATIVE) {
			DPRINT("already negative mode(%d), do not update color temperature(%d)\n",
				mdnie_tune_state->mdnie_accessibility, value);
			return size;
		}

		if (!vdd)
			vdd = mdnie_tune_state->vdd;

		backup = mdnie_tune_state->hmt_color_temperature;
		mdnie_tune_state->hmt_color_temperature = value;

		DPRINT("%s : (%d) -> (%d)\n", __func__, backup, value);
	}

	update_dsi_tcon_mdnie_register(vdd);

	return size;
}

static DEVICE_ATTR(mode, 0664, mode_show, mode_store);
static DEVICE_ATTR(scenario, 0664, scenario_show, scenario_store);
static DEVICE_ATTR(outdce_attribute *attr, char *buf)
{
	int buffer_pos = 0static ssize_t mdnie_ldu_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int buffer_pos = 0;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		buffer_pos += snprintf(buf, 256, "%d %d %d ", mdnie_tune_state->scr_white_red, mdnie_tune_state->scr_white_green, mdnie_tune_state->scr_white_blue);
	}
	return buffer_pos;
}

static ssize_t mdnie_ldu_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t size)
{
	int i, j, idx;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;
	struct mdnie_lite_tun_type *real_mdnie_tune_state = NULL;
	struct dsi_cmd_desc *ldu_tunning_data = NULL;
	struct samsung_display_driver_data *vdd = NULL;

	sscanf(buf, "%d", &idx);

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		real_mdnie_tune_state = mdnie_tune_state;

		if (!vdd)
			vdd = mdnie_tune_state->vdd;

		if (mdnie_tune_state->index == DSI_CTRL_0) {
			if((idx >=0) && (idx < mdnie_data.dsi0_max_adjust_ldu)) {
				for (i = 0; i < MAX_APP_MODE; i++) {
					for (j = 0; j < MAX_MODE; j++) {
						if ((mdnie_data.mdnie_tune_value_dsi0[i][j][0] != NULL) && (i != eBOOK_APP) && (j != READING_MODE)) {
							if(mdnie_data.dsi0_adjust_ldu_table[j] != NULL) {
								ldu_tunning_data = mdnie_data.mdnie_tune_value_dsi0[i][j][0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 1];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 2];
							}
						}
					}
				}
			}
		} else {
			if((idx >=0) && (idx < mdnie_data.dsi1_max_adjust_ldu)) {
				for (i = 0; i < MAX_APP_MODE; i++) {
					for (j = 0; j < MAX_MODE; j++) {
						if ((mdnie_data.mdnie_tune_value_dsi1[i][j][0] != NULL) && (i != eBOOK_APP) && (j != READING_MODE)) {
							if(mdnie_data.dsi1_adjust_ldu_table[j] != NULL) {
								ldu_tunning_data = mdnie_data.mdnie_tune_value_dsi1[i][j][0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 1];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 2];
							}
						}
					}
				}
			}
		}
	}

	update_dsi_tcon_mdnie_register(vdd);
	return size;
}

static ssize_t hmt_color_temperature_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		DPRINT("Current color temperature : %d\n", mdnie_tune_state->hmt_color_temperature);
	}

	return snprintf(buf, 256, "Current color temperature : %d\n", mdnie_tune_state->hmt_color_temperature);
}

static ssize_t hmt_color_temperature_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t size)
{
	int value;
	int backup;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;
	struct samsung_display_driver_data *vdd = NULL;

	sscanf(buf, "%d", &value);

	if (value < HMT_COLOR_TEMP_OFF || value >= HMT_COLOR_TEMP_MAX) {
		DPRINT("[ERROR] wrong color temperature value : %d\n", value);
		return size;
	}

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		if (mdnie_tune_state->mdnie_accessibility == NEGATIVE) {
			DPRINT("already negative mode(%d), do not update color temperature(%d)\n",
				mdnie_tune_state->mdnie_accessibility, value);
			return size;
		}

		if (!vdd)
			vdd = mdnie_tune_state->vdd;

		backup = mdnie_tune_state->hmt_color_temperature;
		mdnie_tune_state->hmt_color_temperature = value;

		DPRINT("%s : (%d) -> (%d)\n", __func__, backup, value);
	}

	update_dsi_tcon_mdnie_register(vdd);

	return size;
}

static DEVICE_ATTR(mode, 0664, mode_show, mode_store);
static DEVICE_ATTR(scenario, 0664, scenario_show, scenario_store);
static DEVICE_ATTR(outd;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		buffer_pos += snprintf(buf, 256, "%d %d %d ", mdnie_tune_state->scr_white_red, mdnie_tune_state->scr_white_green, mdnie_tune_state->scr_white_blue);
	}
	return buffer_pos;
}

static ssize_t mdnie_ldu_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t size)
{
	int i, j, idx;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;
	struct mdnie_lite_tun_type *real_mdnie_tune_state = NULL;
	struct dsi_cmd_desc *ldu_tunning_data = NULL;
	struct samsung_display_driver_data *vdd = NULL;

	sscanf(buf, "%d", &idx);

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		real_mdnie_tune_state = mdnie_tune_state;

		if (!vdd)
			vdd = mdnie_tune_state->vdd;

		if (mdnie_tune_state->index == DSI_CTRL_0) {
			if((idx >=0) && (idx < mdnie_data.dsi0_max_adjust_ldu)) {
				for (i = 0; i < MAX_APP_MODE; i++) {
					for (j = 0; j < MAX_MODE; j++) {
						if ((mdnie_data.mdnie_tune_value_dsi0[i][j][0] != NULL) && (i != eBOOK_APP) && (j != READING_MODE)) {
							if(mdnie_data.dsi0_adjust_ldu_table[j] != NULL) {
								ldu_tunning_data = mdnie_data.mdnie_tune_value_dsi0[i][j][0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 1];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = mdnie_data.dsi0_adjust_ldu_table[j][idx * 3 + 2];
							}
						}
					}
				}
			}
		} else {
			if((idx >=0) && (idx < mdnie_data.dsi1_max_adjust_ldu)) {
				for (i = 0; i < MAX_APP_MODE; i++) {
					for (j = 0; j < MAX_MODE; j++) {
						if ((mdnie_data.mdnie_tune_value_dsi1[i][j][0] != NULL) && (i != eBOOK_APP) && (j != READING_MODE)) {
							if(mdnie_data.dsi1_adjust_ldu_table[j] != NULL) {
								ldu_tunning_data = mdnie_data.mdnie_tune_value_dsi1[i][j][0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 0];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 1];
								ldu_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = mdnie_data.dsi1_adjust_ldu_table[j][idx * 3 + 2];
							}
						}
					}
				}
			}
		}
	}

	update_dsi_tcon_mdnie_register(vdd);
	return size;
}

static ssize_t hmt_color_temperature_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		DPRINT("Current color temperature : %d\n", mdnie_tune_state->hmt_color_temperature);
	}

	return snprintf(buf, 256, "Current color temperature : %d\n", mdnie_tune_state->hmt_color_temperature);
}

static ssize_t hmt_color_temperature_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t size)
{
	int value;
	int backup;
	struct mdnie_lite_tun_type *mdnie_tune_state = NULL;
	struct samsung_display_driver_data *vdd = NULL;

	sscanf(buf, "%d", &value);

	if (value < HMT_COLOR_TEMP_OFF || value >= HMT_COLOR_TEMP_MAX) {
		DPRINT("[ERROR] wrong color temperature value : %d\n", value);
		return size;
	}

	list_for_each_entry_reverse(mdnie_tune_state, &mdnie_list , used_list) {
		if (mdnie_tune_state->mdnie_accessibility == NEGATIVE) {
			DPRINT("already negative mode(%d), do not update color temperature(%d)\n",
				mdnie_tune_state->mdnie_accessibility, value);
			return size;
		}

		if (!vdd)
			vdd = mdnie_tune_state->vdd;

		backup = mdnie_tune_state->hmt_color_temperature;
		mdnie_tune_state->hmt_color_temperature = value;

		DPRINT("%s : (%d) -> (%d)\n", __func__, backup, value);
	}

	update_dsi_tcon_mdnie_register(vdd);

	return size;
}

static DEVICE_ATTR(mode, 0664, mode_show, mode_store);
static DEVICE_ATTR(scenario, 0664, scenario_show, scenario_store);
static DEVICE_ATTR(outdor, 0664, outdoor_show, outdoor_store);
static DEVICE_ATTR(bypass, 0664, bypass_show, bypass_store);
static DEVICE_ATTR(accessibility, 0664, accessibility_show, accessibility_store);
static DEVICE_ATTR(sensorRGB, 0664, sensorRGB_show, sensorRGB_store);
static DEVICE_ATTR(mdnie_ldu, 0664, mdnie_ldu_show, mdnie_ldu_store);
static DEVICE_ATTR(night_mode, 0664, night_mode_show, night_mode_store);
static DEVICE_ATTR(cabc, 0664, cabc_show, cabc_store);
static DEVICE_ATTR(hmt_color_temperature, 0664, hmt_color_temperature_show, hmt_color_temperature_store);

void create_tcon_mdnie_node(void)

{
	struct samsung_display_driver_data *vdd = samsung_get_vdd();

	if (IS_ERR_OR_NULL(vdd)) {
		DPRINT("%s vdd is error", __func__);
	};

	tune_mdnie_dev = device_create(mdnie_class, NULL, 0, NULL,  "mdnie");
	if (IS_ERR(tune_mdnie_dev))
		DPRINT("Failed to create device(mdnie)!\n");

	/* APP */
	if (device_create_file(tune_mdnie_dev, &dev_attr_scenario) < 0)
		DPRINT("Failed to create device file(%s)!\n", dev_attr_scenario.attr.name);

	/* MODE */
	if (device_create_file(tune_mdnie_dev, &dev_attr_mode) < 0)
		DPRINT("Failed to create device file(%s)!\n", dev_attr_mode.attr.name);

	/* OUTDOOR */
	if (device_create_file(tune_mdnie_dev, &dev_attr_outdoor) < 0)
		DPRINT("Failed to create device file(%s)!\n", dev_attr_outdoor.attr.name);

	/* MDNIE ON/OFF */
	if (device_create_file(tune_mdnie_dev, &dev_attr_bypass) < 0)
		DPRINT("Failed to create device file(%s)!\n", dev_attr_bypass.attr.name);
#if 0
	/* NEGATIVE */
	if (device_create_file
		(tune_mdnie_dev, &dev_attr_negative) < 0)
		DPRINT("Failed to create device file(%s)!\n",
			dev_attr_negative.attr.name);
#endif
	/* COLOR BLIND */
	if (device_create_file(tune_mdnie_dev, &dev_attr_accessibility) < 0)
		DPRINT("Failed to create device file(%s)!=n", dev_attr_accessibility.attr.name);

	if (device_create_file
		(tune_mdnie_dev, &dev_attr_sensorRGB) < 0)
		DPRINT("Failed to create device file(%s)!=n",
			dev_attr_sensorRGB.attr.name);

	if (device_create_file
		(tune_mdnie_dev, &dev_attr_mdnie_ldu) < 0)
		DPRINT("Failed to create device file(%s)!=n",
			dev_attr_mdnie_ldu.attr.name);

	if (device_create_file
		(tune_mdnie_dev, &dev_attr_night_mode) < 0)
		DPRINT("Failed to create device file(%s)!=n",
			dev_attr_night_mode.attr.name);

	/* hmt_color_temperature */
	if (device_create_file
		(tune_mdnie_dev, &dev_attr_hmt_color_temperature) < 0)
		DPRINT("Failed to create device file(%s)!=n",
			dev_attr_hmt_color_temperature.attr.name);

	/* CABC ON/OFF */
	if (vdd->support_cabc)
		if (device_create_file(tune_mdnie_dev, &dev_attr_cabc) < 0)
			DPRINT("Failed to create device file(%s)!\n", dev_attr_cabc.attr.name);
}

struct mdnie_lite_tun_type *init_dsi_tcon_mdnie_class(int index, struct samsung_display_driver_data *vdd_data)
{
	struct mdnie_lite_tun_type *mdnie_tune_state;

	if (mdnie_class == NULL) {
		mdnie_class = class_create(THIS_MODULE, "mdnie");

		if (IS_ERR(mdnie_class)) {
			DPRINT("Failed to create class(mdnie)!\n");
			goto out;
		} else
			create_tcon_mdnie_node();

		INIT_LIST_HEAD(&mdnie_list);
	}

	mdnie_tune_state =
		kzalloc(sizeof(struct mdnie_lite_tun_type), GFP_KERNEL);

	if (!mdnie_tune_state) {
		DPRINT("%s allocation fail", __func__);
		goto out;
	} else {
		mdnie_tune_state->vdd = vdd_data;

		mdnie_tune_state->index = index;
		mdnie_tune_state->mdnie_bypass = BYPASS_DISABLE;
		if (mdnie_tune_state->vdd->support_cabc)
			mdnie_tune_state->cabc_bypass = BYPASS_DISABLE;
		mdnie_tune_state->hbm_enable = false;

		mdnie_tune_state->mdnie_app = UI_APP;
		mdnie_tune_state->mdnie_mode = STANDARD_MODE;
		mdnie_tune_state->outdoor = OUTDOOR_OFF_MODE;

		mdnie_tune_state->mdnie_accessibility = ACCESSIBILITY_OFF;

		mdnie_tune_state->scr_white_red = 0xff;
		mdnie_tune_state->scr_white_green = 0xff;
		mdnie_tune_state->scr_white_blue = 0xff;

		mdnie_tune_state->night_mode_enable = false;
		mdnie_tune_state->night_mode_index = 0;

		INIT_LIST_HEAD(&mdnie_tune_state->used_list);

		list_add(&mdnie_tune_state->used_list, &mdnie_list);
	}

	/* Set default link_stats as DSI_HS_MODE for mdnie tune data */
	vdd_data->mdnie_tune_data[index].mdnie_tune_packet_tx_cmds_dsi.link_state = DSI_HS_MODE;

	return mdnie_tune_state;

out:
	return 0;
}

void coordinate_tunning_multi(int index, char (*coordinate_data_multi[MAX_MODE])[COORDINATE_DATA_SIZE], int mdnie_tune_index, int scr_wr_addr, int data_size)
{
	int i, j;
	struct dsi_cmd_desc *coordinate_tunning_data = NULL;

	if (index == DSI_CTRL_0) {
		for (i = 0; i < MAX_APP_MODE; i++) {
			for (j = 0; j < MAX_MODE; j++) {
				if ((mdnie_data.mdnie_tune_value_dsi0[i][j][0] != NULL) && (i != eBOOK_APP) && (j != READING_MODE)) {
					coordinate_tunning_data = mdnie_data.mdnie_tune_value_dsi0[i][j][0];
					coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = coordinate_data_multi[j][mdnie_tune_index][0];
					coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = coordinate_data_multi[j][mdnie_tune_index][2];
					coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = coordinate_data_multi[j][mdnie_tune_index][4];
				}
			}
		}
	} else {
		for (i = 0; i < MAX_APP_MODE; i++) {
			for (j = 0; j < MAX_MODE; j++) {
				if ((mdnie_data.mdnie_tune_value_dsi1[i][j][0] != NULL) && (i != eBOOK_APP) && (j != READING_MODE)) {
					coordinate_tunning_data = mdnie_data.mdnie_tune_value_dsi1[i][j][0];
					coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = coordinate_data_multi[j][mdnie_tune_index][0];
					coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = coordinate_data_multi[j][mdnie_tune_index][2];
					coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = coordinate_data_multi[j][mdnie_tune_index][4];
				}
			}
		}
	}
}

void coordinate_tunning(int index, char *coordinate_data, int scr_wr_addr, int data_size)
{
	int i, j;
	char white_r, white_g, white_b;
	struct dsi_cmd_desc *coordinate_tunning_data = NULL;

	if (index == DSI_CTRL_0) {
		for (i = 0; i < MAX_APP_MODE; i++) {
			for (j = 0; j < MAX_MODE; j++) {
				if (mdnie_data.mdnie_tune_value_dsi0[i][j][0] != NULL) {
					coordinate_tunning_data = mdnie_data.mdnie_tune_value_dsi0[i][j][0];
					white_r = coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]];
					white_g = coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]];
					white_b = coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]];
					if ((white_r == 0xff) && (white_g == 0xff) && (white_b == 0xff)) {
						coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = coordinate_data[0];
						coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = coordinate_data[2];
						coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = coordinate_data[4];
					}
				}
			}
		}
	} else {
		for (i = 0; i < MAX_APP_MODE; i++) {
			for (j = 0; j < MAX_MODE; j++) {
				if (mdnie_data.mdnie_tune_value_dsi1[i][j][0] != NULL) {
					coordinate_tunning_data = mdnie_data.mdnie_tune_value_dsi1[i][j][0];
					white_r = coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]];
					white_g = coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]];
					white_b = coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]];
					if ((white_r == 0xff) && (white_g == 0xff) && (white_b == 0xff)) {
						coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_RED_OFFSET]] = coordinate_data[0];
						coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_GREEN_OFFSET]] = coordinate_data[2];
						coordinate_tunning_data[mdnie_data.mdnie_step_index[MDNIE_STEP2]].payload[mdnie_data.address_scr_white[ADDRESS_SCR_WHITE_BLUE_OFFSET]] = coordinate_data[4];
					}
				}
			}
		}
	}
}

void print_tun_data(void)
{
	int i;
	DPRINT("\n");
	DPRINT("---- size1 : %d", PAYLOAD1.dchdr.dlen);
	for (i = 0; i < MDNIE_TUNE_FIRST_SIZE ; i++)
		DPRINT("0x%x ", PAYLOAD1.payload[i]);
	DPRINT("\n");
	DPRINT("---- size2 : %d", PAYLOAD2.dchdr.dlen);
	for (i = 0; i < MDNIE_TUNE_SECOND_SIZE ; i++)
		DPRINT("0x%x ", PAYLOAD2.payload[i]);
	DPRINT("\n");
}

void free_tun_cmd(void)
{
	memset(mdnie_data_1, 0, MDNIE_TUNE_FIRST_SIZE);
	memset(mdnie_data_2, 0, MDNIE_TUNE_SECOND_SIZE);
}

static unsigned char get_ascr(unsigned char* tune_value, int address)
{
	unsigned short* temp = (unsigned short *)&tune_value[address];
	if (tune_value)
		return (unsigned char)(be16_to_cpu(*temp) >> BITSHIFT_SCR);
	else
		return ENODEV;
}

static void set_ascr(unsigned char* tune_value, int address, char value)
{
	unsigned short* temp = (unsigned short *)&tune_value[address];

	*temp &= ~(cpu_to_be16(0xFF << BITSHIFT_SCR));
	*temp |= cpu_to_be16(((u16)value) << BITSHIFT_SCR);
}

void sending_tuning_cmd(void)
{
	struct msm_fb_data_type *mfd;
	struct mdss_dsi_ctrl_pdata *ctrl_pdata;

	mfd = mdnie_msd->mfd;
	ctrl_pdata = mdnie_msd->ctrl_pdata;

	if (mfd->resume_state == MIPI_SUSPEND_STATE) {
		DPRINT("[ERROR] not ST_DSI_RESUME. do not send mipi cmd.\n");
		return;
	}

#if 0
	if (mdnie_adb_test) {
		DPRINT("[ERROR] mdnie_adb_test is doning .. copy from adb data .. \n");
		INPUT_PAYLOAD1(tune_data2_adb);
		INPUT_PAYLOAD2(tune_data1_adb);
	}
#endif

	mutex_lock(&mdnie_msd->lock);

#ifdef MDNIE_LITE_TUN_DATA_DEBUG
	print_tun_data();
#endif
	mdss_dsi_cmds_send(ctrl_pdata, mdni_enable_cmd, ARRAY_SIZE(mdni_enable_cmd),0);
	mdss_dsi_cmds_send(ctrl_pdata, mdni_tune_cmd, ARRAY_SIZE(mdni_tune_cmd),0);

	mutex_unlock(&mdnie_msd->lock);
	}
/*
 * mDnie priority
 * Accessibility > HBM > Screen Mode
 */
void mDNIe_Set_Mode(void)
{
	struct msm_fb_data_type *mfd;
	unsigned char* ascr;
	mfd = mdnie_msd->mfd;

/*	DPRINT("mDNIe_Set_Mode start\n");*/

	if (!mfd) {
		DPRINT("[ERROR] mfd is null!\n");
		return;
	}

#if defined(CONFIG_LCD_FORCE_VIDEO_MODE)
	DPRINT("mDNIe_Set_Mode start : return cause of video mode\n");
	return;
#endif

	if (mfd->blank_mode) {
		DPRINT("[ERROR] blank_mode (%d). do not send mipi cmd.\n",
			mfd->blank_mode);
		return;
	}

	if (mfd->resume_state == MIPI_SUSPEND_STATE) {
		DPRINT("[ERROR] not ST_DSI_RESUME. do not send mipi cmd.\n");
		return;
	}

	if (!mdnie_tun_state.mdnie_enable) {
		DPRINT("[ERROR] mDNIE engine is OFF.\n");
		return;
	}

	if (mdnie_tun_state.scenario < mDNIe_UI_MODE || mdnie_tun_state.scenario >= MAX_mDNIe_MODE) {
		DPRINT("[ERROR] wrong Scenario mode value : %d\n",
			mdnie_tun_state.scenario);
		return;
	}

	play_speed_1_5 = 0;

	if (mdnie_tun_state.accessibility) {
		DPRINT(" = ACCESSIBILITY MODE =\n");
		INPUT_PAYLOAD1(blind_tune_value[mdnie_tun_state.accessibility][0]);
		INPUT_PAYLOAD2(blind_tune_value[mdnie_tun_state.accessibility][1]);
	}

	else if (mdnie_msd->dstat.auto_brightness >= 6 && mdnie_msd->dstat.bright_level == 255) {
		DPRINT("[LOCAL CE] HBM mode! only LOCAL CE tuning\n");
		if((mdnie_tun_state.scenario == mDNIe_BROWSER_MODE)||(mdnie_tun_state.scenario == mDNIe_eBOOK_MODE)) {
			INPUT_PAYLOAD1(LOCAL_CE_TEXT_1);
			INPUT_PAYLOAD2(LOCAL_CE_TEXT_2);
		}
		else {
			INPUT_PAYLOAD1(LOCAL_CE_1);
			INPUT_PAYLOAD2(LOCAL_CE_2);
	}
	}

#if defined(CONFIG_TDMB)
	else if (mdnie_tun_state.dmb > DMB_MODE_OFF){
		if (!dmb_tune_value[mdnie_tun_state.dmb][mdnie_tun_state.background][mdnie_tun_state.outdoor][0] ||
			!dmb_tune_value[mdnie_tun_state.dmb][mdnie_tun_state.background][mdnie_tun_state.outdoor][1]) {
			pr_err("dmb tune data is NULL!\n");
			return;
		} else {
			INPUT_PAYLOAD1(
				dmb_tune_value[mdnie_tun_state.dmb][mdnie_tun_state.background][mdnie_tun_state.outdoor][0]);
			INPUT_PAYLOAD2(
				dmb_tune_value[mdnie_tun_state.dmb][mdnie_tun_state.background][mdnie_tun_state.outdoor][1]);
		}
	}
#endif
	else {
		if (!mdnie_tune_value[mdnie_tun_state.scenario][mdnie_tun_state.background][mdnie_tun_state.outdoor][0] ||
			!mdnie_tune_value[mdnie_tun_state.scenario][mdnie_tun_state.background][mdnie_tun_state.outdoor][1]) {
			pr_err("mdnie tune data is NULL!\n");
			return;
		} else {
			INPUT_PAYLOAD1(
				mdnie_tune_value[mdnie_tun_state.scenario][mdnie_tun_state.background][mdnie_tun_state.outdoor][0]);
			INPUT_PAYLOAD2(
				mdnie_tune_value[mdnie_tun_state.scenario][mdnie_tun_state.background][mdnie_tun_state.outdoor][1]);
			ascr = mdnie_tune_value[mdnie_tun_state.scenario][mdnie_tun_state.background][mdnie_tun_state.outdoor][1];
			mdnie_tun_state.scr_white_red = get_ascr(ascr, ADDRESS_SCR_WHITE_RED);
			mdnie_tun_state.scr_white_green = get_ascr(ascr, ADDRESS_SCR_WHITE_GREEN);
			mdnie_tun_state.scr_white_blue = get_ascr(ascr, ADDRESS_SCR_WHITE_BLUE);
			ascr = NULL;
		}
}

	sending_tuning_cmd();
	free_tun_cmd();

	DPRINT("mDNIe_Set_Mode end , %s(%d), %s(%d), %s(%d), %s(%d)\n",
		scenario_name[mdnie_tun_state.scenario], mdnie_tun_state.scenario,
		background_name[mdnie_tun_state.background], mdnie_tun_state.background,
		outdoor_name[mdnie_tun_state.outdoor], mdnie_tun_state.outdoor,
		accessibility_name[mdnie_tun_state.accessibility], mdnie_tun_state.accessibility);

}

void is_play_speed_1_5(int enable)
{
	play_speed_1_5 = enable;
}

/* ##########################################################
 * #
 * # MDNIE BG Sysfs node
 * #
 * ##########################################################*/

/* ##########################################################
 * #
 * #	0. Dynamic
 * #	1. Standard
 * #	2. Video
 * #	3. Natural
 * #
 * ##########################################################*/

static ssize_t mode_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	DPRINT("Current Background Mode : %s\n",
		background_name[mdnie_tun_state.background]);

	return snprintf(buf, 256, "Current Background Mode : %s\n",
		background_name[mdnie_tun_state.background]);
}

static ssize_t mode_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t size)
{
	int value;
	int backup;

	sscanf(buf, "%d", &value);

	if (value < DYNAMIC_MODE || value >= MAX_BACKGROUND_MODE) {
		DPRINT("[ERROR] wrong backgound mode value : %d\n",
			value);
		return size;
	}
	backup = mdnie_tun_state.background;
	mdnie_tun_state.background = value;

	if (mdnie_tun_state.accessibility == NEGATIVE) {
		DPRINT("already negative mode(%d), do not set background(%d)\n",
			mdnie_tun_state.accessibility, mdnie_tun_state.background);
	} else {
		DPRINT(" %s : (%s) -> (%s)\n",
			__func__, background_name[backup], background_name[mdnie_tun_state.background]);

		mDNIe_Set_Mode();
	}

	return size;
}

static DEVICE_ATTR(mode, 0664, mode_show, mode_store);

static ssize_t scenario_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	DPRINT("Current Scenario Mode : %s\n",
		scenario_name[mdnie_tun_state.scenario]);

	return snprintf(buf, 256, "Current Scenario Mode : %s\n",
		scenario_name[mdnie_tun_state.scenario]);
}

static ssize_t scenario_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t size)
{
	int value;
	int backup;

	sscanf(buf, "%d", &value);

	if (value < mDNIe_UI_MODE || value >= MAX_mDNIe_MODE) {
		DPRINT("[ERROR] wrong Scenario mode value : %d\n",
			value);
		return size;
	}

	backup = mdnie_tun_state.scenario;
	mdnie_tun_state.scenario = value;

#if defined(CONFIG_TDMB)
	/* mDNIe_DMB_MODE = 20 */
	if (value >= mDNIe_DMB_MODE && value <= mDNIe_DMB_COLD_MODE) {
		DPRINT("DMB scenario.. (%d)\n", mdnie_tun_state.scenario);
		mdnie_tun_state.dmb = value - mDNIe_DMB_MODE;
	} else
		mdnie_tun_state.dmb = DMB_MODE_OFF;
#endif

	if (mdnie_tun_state.accessibility == NEGATIVE) {
		DPRINT("already negative mode(%d), do not set mode(%d)\n",
			mdnie_tun_state.accessibility, mdnie_tun_state.scenario);
	} else {
		DPRINT(" %s : (%s) -> (%s)\n",
			__func__, scenario_name[backup], scenario_name[mdnie_tun_state.scenario]);
		mDNIe_Set_Mode();
	}
	return size;
}
static DEVICE_ATTR(scenario, 0664, scenario_show,
		   scenario_store);

static ssize_t mdnieset_user_select_file_cmd_show(struct device *dev,
						  struct device_attribute *attr,
						  char *buf)
{
	int mdnie_ui = 0;
	DPRINT("called %s\n", __func__);

	return snprintf(buf, 256, "%u\n", mdnie_ui);
}

static ssize_t mdnieset_user_select_file_cmd_store(struct device *dev,
						   struct device_attribute
						   *attr, const char *buf,
						   size_t size)
{
	int value;

	sscanf(buf, "%d", &value);
	DPRINT
	("inmdnieset_user_select_file_cmd_store, input value = %d\n",
	     value);

	return size;
}

static DEVICE_ATTR(mdnieset_user_select_file_cmd, 0664,
		   mdnieset_user_select_file_cmd_show,
		   mdnieset_user_select_file_cmd_store);

static ssize_t mdnieset_init_file_cmd_show(struct device *dev,
					   struct device_attribute *attr,
					   char *buf)
{
	char temp[] = "mdnieset_init_file_cmd_show\n\0";
	DPRINT("called %s\n", __func__);
	strcat(buf, temp);
	return strlen(buf);
}

static ssize_t mdnieset_init_file_cmd_store(struct device *dev,
					    struct device_attribute *attr,
					    const char *buf, size_t size)
{
	int value;

	sscanf(buf, "%d", &value);
	DPRINT("mdnieset_init_file_cmd_store  : value(%d)\n", value);

	switch (value) {
	case 0:
		mdnie_tun_state.scenario = mDNIe_UI_MODE;
		break;

	default:
		printk(KERN_ERR
		       "mdnieset_init_file_cmd_store value is wrong : value(%d)\n",
		       value);
		break;
	}
	mDNIe_Set_Mode();

	return size;
}

static DEVICE_ATTR(mdnieset_init_file_cmd, 0664, mdnieset_init_file_cmd_show,
		   mdnieset_init_file_cmd_store);

static ssize_t outdoor_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	DPRINT("Current outdoor Mode : %s\n",
		outdoor_name[mdnie_tun_state.outdoor]);

	return snprintf(buf, 256, "Current outdoor Mode : %s\n",
		outdoor_name[mdnie_tun_state.outdoor]);
}

static ssize_t outdoor_store(struct device *dev,
					       struct device_attribute *attr,
					       const char *buf, size_t size)
{
	int value;
	int backup;

	sscanf(buf, "%d", &value);

	DPRINT("outdoor value = %d, scenario = %d\n",
		value, mdnie_tun_state.scenario);

	if (value < OUTDOOR_OFF_MODE || value >= MAX_OUTDOOR_MODE) {
		DPRINT("[ERROR] : wrong outdoor mode value : %d\n",
				value);
	}

	backup = mdnie_tun_state.outdoor;
	mdnie_tun_state.outdoor = value;

	if (mdnie_tun_state.accessibility == NEGATIVE) {
		DPRINT("already negative mode(%d), do not outdoor mode(%d)\n",
			mdnie_tun_state.accessibility, mdnie_tun_state.outdoor);
	} else {
		DPRINT(" %s : (%s) -> (%s)\n",
			__func__, outdoor_name[backup], outdoor_name[mdnie_tun_state.outdoor]);
		mDNIe_Set_Mode();
	}

	return size;
}

static DEVICE_ATTR(outdoor, 0664, outdoor_show, outdoor_store);

#if 0 // accessibility
static ssize_t negative_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	return snprintf(buf, 256, "Current negative Value : %s\n",
		(mdnie_tun_state.accessibility == 1) ? "Enabled" : "Disabled");
}

static ssize_t negative_store(struct device *dev,
					       struct device_attribute *attr,
					       const char *buf, size_t size)
{
	int value;

	sscanf(buf, "%d", &value);

	DPRINT
	    ("negative_store, input value = %d\n",
	     value);

	mdnie_tun_state.accessibility = value;

	mDNIe_Set_Mode();

	return size;
}
static DEVICE_ATTR(negative, 0664,
		   negative_show,
		   negative_store);

#endif

void is_negative_on(void)
{
	DPRINT("is negative Mode On = %d\n", mdnie_tun_state.accessibility);

	mDNIe_Set_Mode();
}

static ssize_t playspeed_show(struct device *dev,
			struct device_attribute *attr,
			char *buf)
{
	DPRINT("called %s\n", __func__);
	return snprintf(buf, 256, "%d\n", play_speed_1_5);
}

static ssize_t playspeed_store(struct device *dev,
			struct device_attribute *attr,
			const char *buf, size_t size)
{
	int value;
	sscanf(buf, "%d", &value);

	DPRINT("[Play Speed Set]play speed value = %d\n", value);

	is_play_speed_1_5(value);
	return size;
}
static DEVICE_ATTR(playspeed, 0664,
			playspeed_show,
			playspeed_store);

static ssize_t accessibility_show(struct device *dev,
			struct device_attribute *attr,
			char *buf)
{
	DPRINT("Current accessibility Mode : %s\n",
		accessibility_name[mdnie_tun_state.accessibility]);

	return snprintf(buf, 256, "Current accessibility Mode : %s\n",
		accessibility_name[mdnie_tun_state.accessibility]);
}

static ssize_t accessibility_store(struct device *dev,
			struct device_attribute *attr,
			const char *buf, size_t size)
{
	int cmd_value;
	char buffer[MDNIE_COLOR_BLINDE_CMD] = {0,};
	int buffer2[MDNIE_COLOR_BLINDE_CMD/2] = {0,};
	int loop;
	char temp;
	int backup;

	sscanf(buf, "%d %x %x %x %x %x %x %x %x %x", &cmd_value,
		&buffer2[0], &buffer2[1], &buffer2[2], &buffer2[3], &buffer2[4],
		&buffer2[5], &buffer2[6], &buffer2[7], &buffer2[8]);

	backup = mdnie_tun_state.accessibility;

	if (cmd_value == NEGATIVE) {
		mdnie_tun_state.accessibility = NEGATIVE;
	} else if (cmd_value == COLOR_BLIND) {
		mdnie_tun_state.accessibility = COLOR_BLIND;

		memcpy(buffer,&COLOR_BLIND_2[MDNIE_COLOR_BLINDE_OFFSET],
				MDNIE_COLOR_BLINDE_CMD);

	for(loop = 0; loop < MDNIE_COLOR_BLINDE_CMD/2; loop++) {
		buffer2[loop] = buffer2[loop] & 0xFFFF;

			temp = buffer[loop * 2];
			temp &= 0xFC;
			temp |= ((buffer2[loop] & 0xFF) >> 6) & 0x03;
			buffer[loop * 2] = temp;

			temp = buffer[loop * 2 + 1];
			temp &= 0x03;
			temp |= ((buffer2[loop] & 0xFF) << 2) & 0xFC;
			temp &= 0xFC;
			temp |= (((buffer2[loop] & 0xFF00) >> 8) >> 6) & 0x03;
			buffer[loop * 2 + 1] = temp;

			temp = buffer[loop * 2 + 2];
			temp &= 0x03;
			temp |= (((buffer2[loop] & 0xFF00) >> 8) << 2) & 0xFC;
			buffer[loop * 2 + 2] = temp;
	}

		memcpy(&COLOR_BLIND_2[MDNIE_COLOR_BLINDE_OFFSET],
				buffer, MDNIE_COLOR_BLINDE_CMD);
	} else if (cmd_value == SCREEN_CURTAIN) {
		mdnie_tun_state.accessibility = SCREEN_CURTAIN;
	} else if (cmd_value == ACCESSIBILITY_OFF) {
		mdnie_tun_state.accessibility = ACCESSIBILITY_OFF;
	} else
		pr_info("%s ACCESSIBILITY_MAX", __func__);

	DPRINT(" %s : (%s) -> (%s)\n",
			__func__, accessibility_name[backup], accessibility_name[mdnie_tun_state.accessibility]);

	mDNIe_Set_Mode();

	pr_info("%s cmd_value : %d size : %d", __func__, cmd_value, size);

	return size;
}

static DEVICE_ATTR(accessibility, 0664,
			accessibility_show,
			accessibility_store);

static ssize_t sensorRGB_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
		return sprintf(buf, "%d %d %d\n", mdnie_tun_state.scr_white_red, mdnie_tun_state.scr_white_green, mdnie_tun_state.scr_white_blue);
}

static ssize_t sensorRGB_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t size)
{
	int red, green, blue;
	char white_red, white_green, white_blue;

	sscanf(buf, "%d %d %d", &red, &green, &blue);

	if ((mdnie_tun_state.accessibility == ACCESSIBILITY_OFF) && (mdnie_tun_state.background == AUTO_MODE) &&	\
		((mdnie_tun_state.scenario == mDNIe_BROWSER_MODE) || (mdnie_tun_state.scenario == mDNIe_eBOOK_MODE)))
	{
		white_red = (char)(red);
		white_green = (char)(green);
		white_blue= (char)(blue);
		mdnie_tun_state.scr_white_red = red;
		mdnie_tun_state.scr_white_green = green;
		mdnie_tun_state.scr_white_blue= blue;
		DPRINT("%s: white_red = %d, white_green = %d, white_blue = %d\n", __func__, white_red, white_green, white_blue);

		INPUT_PAYLOAD1(mdnie_tune_value[mdnie_tun_state.scenario][mdnie_tun_state.background][mdnie_tun_state.outdoor][0]);
		memcpy(white_rgb_buf, mdnie_tune_value[mdnie_tun_state.scenario][mdnie_tun_state.background][mdnie_tun_state.outdoor][1], MDNIE_TUNE_FIRST_SIZE);

		white_rgb_buf[ADDRESS_SCR_WHITE_RED] = white_red;
		white_rgb_buf[ADDRESS_SCR_WHITE_GREEN] = white_green;
		white_rgb_buf[ADDRESS_SCR_WHITE_BLUE] = white_blue;

		INPUT_PAYLOAD2(white_rgb_buf);
		sending_tuning_cmd();
		free_tun_cmd();
	}

	return size;
}

static DEVICE_ATTR(sensorRGB, 0664, sensorRGB_show, sensorRGB_store);


#if defined(CONFIG_FB_MSM_MDSS_MDP3)
#if defined(DDI_VIDEO_ENHANCE_TUNING)
#define MAX_FILE_NAME	128
#define TUNING_FILE_PATH "/sdcard/"
static char tuning_file[MAX_FILE_NAME];

static char char_to_dec(char data1, char data2)
{
	char dec;

	dec = 0;

	if (data1 >= 'a') {
		data1 -= 'a';
		data1 += 10;
	} else if (data1 >= 'A') {
		data1 -= 'A';
		data1 += 10;
	} else
		data1 -= '0';

	dec = data1 << 4;

	if (data2 >= 'a') {
		data2 -= 'a';
		data2 += 10;
	} else if (data2 >= 'A') {
		data2 -= 'A';
		data2 += 10;
	} else
		data2 -= '0';

	dec |= data2;

	return dec;
}

static void sending_tune_cmd(char *src, int len)
{
	int data_pos = 0;
	int cmd_step = 0;
	int cmd_pos = 0;
	int tbl_cnt = 0;

	for (data_pos = 0; data_pos < len;) {
		if (*(src + data_pos) == '0') {
			if (*(src + data_pos + 1) == 'x') {
				data_pos += 3;
				cmd_pos++;

				if (cmd_pos == tune_size_tbl[tbl_cnt] && cmd_step == tbl_cnt) {
					cmd_pos = 0;
					cmd_step++;
					tbl_cnt++;
				}
			} else
				data_pos++;
		} else {
			data_pos++;
		}
	}


	print_tun_data();

	sending_tuning_cmd();
	free_tun_cmd();
}

static void load_tuning_file(char *filename)
{
	struct file *filp;
	char *dp;
	long l;
	loff_t pos;
	int ret;
	mm_segment_t fs;

	pr_info("%s called loading file name : [%s]\n", __func__,
	       filename);

	fs = get_fs();
	set_fs(get_ds());

	filp = filp_open(filename, O_RDONLY, 0);
	if (IS_ERR(filp)) {
		printk(KERN_ERR "%s File open failed\n", __func__);
		return;
	}

	l = filp->f_path.dentry->d_inode->i_size;
	pr_info("%s Loading File Size : %ld(bytes)", __func__, l);

	dp = kmalloc(l + 10, GFP_KERNEL);
	if (dp == NULL) {
		pr_info("Can't not alloc memory for tuning file load\n");
		filp_close(filp, current->files);
		return;
	}
	pos = 0;
	memset(dp, 0, l);

	pr_info("%s before vfs_read()\n", __func__);
	ret = vfs_read(filp, (char __user *)dp, l, &pos);
	pr_info("%s after vfs_read()\n", __func__);

	if (ret != l) {
		pr_info("vfs_read() filed ret : %d\n", ret);
		kfree(dp);
		filp_close(filp, current->files);
		return;
	}

	filp_close(filp, current->files);

	set_fs(fs);

	sending_tune_cmd(dp, l);

	kfree(dp);
}

static ssize_t tuning_show(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	int ret = 0;

	ret = snprintf(buf, MAX_FILE_NAME, "tuned file name : %s\n", tuning_file);

	return ret;
}

static ssize_t tuning_store(struct device *dev,
			struct device_attribute *attr, const char *buf, size_t size)
{
	char *pt;
	memset(tuning_file, 0, sizeof(tuning_file));
	snprintf(tuning_file, MAX_FILE_NAME, "%s%s", TUNING_FILE_PATH, buf);

	pt = tuning_file;
	while (*pt) {
		if (*pt == '\r' || *pt == '\n') {
			*pt = 0;
			break;
		}
		pt++;
	}

	DPRINT("%s\n", tuning_file);

	load_tuning_file(tuning_file);

	return size;
}
static DEVICE_ATTR(tuning, S_IRUGO | S_IWUSR | S_IWGRP,
						tuning_show,
						tuning_store);

#endif /* DDI_VIDEO_ENHANCE_TUNING */
#endif /* CONFIG_FB_MSM_MDSS_MDP3 */

static struct class *mdnie_class;
struct device *tune_mdnie_dev;

void init_mdnie_class(void)
{
	if (mdnie_tun_state.mdnie_enable) {
		pr_err("%s : mdnie already enable.. \n",__func__);
		return;
	}

	DPRINT("start!\n");

	mdnie_class = class_create(THIS_MODULE, "mdnie");
	if (IS_ERR(mdnie_class))
		pr_err("Failed to create class(mdnie)!\n");

	tune_mdnie_dev =
	    device_create(mdnie_class, NULL, 0, NULL,
		  "mdnie");
	if (IS_ERR(tune_mdnie_dev))
		pr_err("Failed to create device(mdnie)!\n");

	if (device_create_file
	    (tune_mdnie_dev, &dev_attr_scenario) < 0)
		pr_err("Failed to create device file(%s)!\n",
	       dev_attr_scenario.attr.name);

	if (device_create_file
	    (tune_mdnie_dev,
	     &dev_attr_mdnieset_user_select_file_cmd) < 0)
		pr_err("Failed to create device file(%s)!\n",
			dev_attr_mdnieset_user_select_file_cmd.attr.name);

	if (device_create_file
	    (tune_mdnie_dev, &dev_attr_mdnieset_init_file_cmd) < 0)
		pr_err("Failed to create device file(%s)!\n",
			dev_attr_mdnieset_init_file_cmd.attr.name);

	if (device_create_file
		(tune_mdnie_dev, &dev_attr_mode) < 0)
		pr_err("Failed to create device file(%s)!\n",
			dev_attr_mode.attr.name);

	if (device_create_file
		(tune_mdnie_dev, &dev_attr_outdoor) < 0)
		pr_err("Failed to create device file(%s)!\n",
	       dev_attr_outdoor.attr.name);

#if 0 // accessibility
	if (device_create_file
		(tune_mdnie_dev, &dev_attr_negative) < 0)
		pr_err("Failed to create device file(%s)!\n",
			dev_attr_negative.attr.name);
#endif

	if (device_create_file
		(tune_mdnie_dev, &dev_attr_playspeed) < 0)
		pr_err("Failed to create device file(%s)!=n",
			dev_attr_playspeed.attr.name);

	if (device_create_file
		(tune_mdnie_dev, &dev_attr_accessibility) < 0)
		pr_err("Failed to create device file(%s)!=n",
			dev_attr_accessibility.attr.name);

	if (device_create_file
		(tune_mdnie_dev, &dev_attr_sensorRGB) < 0)
		pr_err("Failed to create device file(%s)!=n",
			dev_attr_sensorRGB.attr.name);


#if defined(CONFIG_FB_MSM_MDSS_MDP3)
#if defined(DDI_VIDEO_ENHANCE_TUNING)
	if (device_create_file
		(tune_mdnie_dev, &dev_attr_tuning) < 0)
		pr_err("Failed to create device file(%s)!=n",
			dev_attr_tuning.attr.name);
#endif
#endif

	mdnie_tun_state.mdnie_enable = true;

#if defined(CONFIG_TDMB)
	strcpy((char*) scenario_name[mDNIe_DMB_MODE], "DMB_MODE");
	strcpy((char*) scenario_name[mDNIe_DMB_WARM_MODE], "DMB_WARM_MODE");
	strcpy((char*) scenario_name[mDNIe_DMB_COLD_MODE], "DMB_COLD_MODE");
#endif

	DPRINT("end!\n");
}

void mdnie_lite_tuning_init(struct mipi_samsung_driver_data *msd)
{
	mdnie_msd = msd;
}

void coordinate_tunning(int x, int y)
{
	int tune_number;
	int loop_mode, loop_bg;
	unsigned char* tune_value;

	tune_number = 0;

	if (F1(x,y) > 0) {
		if (F3(x,y) > 0) {
			tune_number = 3;
		} else {
			if (F4(x,y) < 0)
				tune_number = 1;
			else
				tune_number = 2;
		}
	} else {
		if (F2(x,y) < 0) {
			if (F3(x,y) > 0) {
				tune_number = 9;
			} else {
				if (F4(x,y) < 0)
					tune_number = 7;
				else
					tune_number = 8;
			}
		} else {
			if (F3(x,y) > 0)
				tune_number = 6;
			else {
				if (F4(x,y) < 0)
					tune_number = 4;
				else
					tune_number = 5;
			}
		}
	}

	pr_info("%s x : %d, y : %d, tune_number : %d", __func__, x, y, tune_number);

	for(loop_mode=0; loop_mode < MAX_mDNIe_MODE ; loop_mode++) {
		for(loop_bg=0; loop_bg < MAX_BACKGROUND_MODE ; loop_bg++) {
			tune_value = mdnie_tune_value[loop_mode][loop_bg][OUTDOOR_OFF_MODE][1];
			if (IS_ERR_OR_NULL(tune_value))
				continue;
			if((get_ascr(tune_value, ADDRESS_SCR_WHITE_RED) == 0xff)
				&& (get_ascr(tune_value, ADDRESS_SCR_WHITE_GREEN) == 0xff)
				&& (get_ascr(tune_value, ADDRESS_SCR_WHITE_BLUE) == 0xff)) {
				set_ascr(tune_value, ADDRESS_SCR_WHITE_RED, coordinate_data[tune_number][0]);
				set_ascr(tune_value, ADDRESS_SCR_WHITE_GREEN, coordinate_data[tune_number][1]);
				set_ascr(tune_value, ADDRESS_SCR_WHITE_BLUE, coordinate_data[tune_number][2]);
			}
		}
	}
#if 0
	if(get_ascr(DYNAMIC_BROWSER_2, ADDRESS_SCR_WHITE_RED) == 0xFF)
		set_ascr(DYNAMIC_BROWSER_2, ADDRESS_SCR_WHITE_RED, coordinate_data[tune_number][0]);
	memcpy(&DYNAMIC_BROWSER_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&DYNAMIC_GALLERY_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&DYNAMIC_UI_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&DYNAMIC_VIDEO_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&DYNAMIC_VT_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&DYNAMIC_EBOOK_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);

	memcpy(&STANDARD_BROWSER_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&STANDARD_GALLERY_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&STANDARD_UI_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&STANDARD_VIDEO_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&STANDARD_VT_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&STANDARD_EBOOK_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);

	memcpy(&AUTO_BROWSER_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&AUTO_CAMERA_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&AUTO_GALLERY_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&AUTO_UI_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&AUTO_VIDEO_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
	memcpy(&AUTO_VT_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);

	memcpy(&CAMERA_2[scr_wr_addr], &coordinate_data[tune_number][0], coordinate_data_size);
#endif

}

#if 0
void mDNIe_Set_Mode(enum Lcd_mDNIe_UI mode)
{
	struct msm_fb_data_type *mfd;
	mfd = mdnie_msd->mfd;

	DPRINT("mDNIe_Set_Mode start , mode(%d), background(%d)\n",
		mode, mdnie_tun_state.background);

	if (!mfd) {
		DPRINT("[ERROR] mfd is null!\n");
		return;
	}

	if (mfd->resume_state == MIPI_SUSPEND_STATE) {
		DPRINT("[ERROR] not ST_DSI_RESUME. do not send mipi cmd.\n");
		return;
	}

	if (!mdnie_tun_state.mdnie_enable) {
		DPRINT("[ERROR] mDNIE engine is OFF.\n");
		return;
	}

	if (mode < mDNIe_UI_MODE || mode >= MAX_mDNIe_MODE) {
		DPRINT("[ERROR] wrong Scenario mode value : %d\n",
			mode);
		return;
	}

	if (mdnie_tun_state.negative) {
		DPRINT("already negative mode(%d), do not set background(%d)\n",
			mdnie_tun_state.negative, mdnie_tun_state.background);
		return;
	}

	play_speed_1_5 = 0;

	/*
	*	Blind mode & Screen mode has separated menu.
	*	To make a sync below code added.
	*	Bline mode has priority than Screen mode
	*/
	if (mdnie_tun_state.accessibility == COLOR_BLIND)
		mode = mDNIE_BLINE_MODE;
#if defined(CONFIG_FB_MSM_MIPI_SAMSUNG_OCTA_CMD_WQHD_PT_PANEL)
	if (get_lcd_panel_res() == 0) { // 0 : wqhd
#endif

	switch (mode) {
	case mDNIe_UI_MODE:
		DPRINT(" = UI MODE =\n");
		if (mdnie_tun_state.background == STANDARD_MODE) {
			DPRINT(" = STANDARD MODE =\n");
			INPUT_PAYLOAD1(STANDARD_UI_1);
			INPUT_PAYLOAD2(STANDARD_UI_2);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
		} else if (mdnie_tun_state.background == NATURAL_MODE) {
			DPRINT(" = NATURAL MODE =\n");
			INPUT_PAYLOAD1(NATURAL_UI_1);
			INPUT_PAYLOAD2(NATURAL_UI_2);
#endif
		} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
			DPRINT(" = DYNAMIC MODE =\n");
			INPUT_PAYLOAD1(DYNAMIC_UI_1);
			INPUT_PAYLOAD2(DYNAMIC_UI_2);
		} else if (mdnie_tun_state.background == MOVIE_MODE) {
			DPRINT(" = MOVIE MODE =\n");
			INPUT_PAYLOAD1(MOVIE_UI_1);
			INPUT_PAYLOAD2(MOVIE_UI_2);
		} else if (mdnie_tun_state.background == AUTO_MODE) {
			DPRINT(" = AUTO MODE =\n");
			INPUT_PAYLOAD1(AUTO_UI_1);
			INPUT_PAYLOAD2(AUTO_UI_2);
		}
		break;

	case mDNIe_VIDEO_MODE:
		DPRINT(" = VIDEO MODE =\n");
		if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
			DPRINT(" = OUTDOOR ON MODE =\n");
			INPUT_PAYLOAD1(OUTDOOR_VIDEO_1);
			INPUT_PAYLOAD2(OUTDOOR_VIDEO_2);
		} else if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
			DPRINT(" = OUTDOOR OFF MODE =\n");
			if (mdnie_tun_state.background == STANDARD_MODE) {
				DPRINT(" = STANDARD MODE =\n");
				INPUT_PAYLOAD1(STANDARD_VIDEO_1);
				INPUT_PAYLOAD2(STANDARD_VIDEO_2);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
			} else if (mdnie_tun_state.background == NATURAL_MODE) {
				DPRINT(" = NATURAL MODE =\n");
				INPUT_PAYLOAD1(NATURAL_VIDEO_1);
				INPUT_PAYLOAD2(NATURAL_VIDEO_2);
#endif
			} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
				DPRINT(" = DYNAMIC MODE =\n");
				INPUT_PAYLOAD1(DYNAMIC_VIDEO_1);
				INPUT_PAYLOAD2(DYNAMIC_VIDEO_2);
			} else if (mdnie_tun_state.background == MOVIE_MODE) {
				DPRINT(" = MOVIE MODE =\n");
				INPUT_PAYLOAD1(MOVIE_VIDEO_1);
				INPUT_PAYLOAD2(MOVIE_VIDEO_2);
			} else if (mdnie_tun_state.background == AUTO_MODE) {
				DPRINT(" = AUTO MODE =\n");
				INPUT_PAYLOAD1(AUTO_VIDEO_1);
				INPUT_PAYLOAD2(AUTO_VIDEO_2);
			}
		}
		break;

	case mDNIe_VIDEO_WARM_MODE:
		DPRINT(" = VIDEO WARM MODE =\n");
		if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
			DPRINT(" = OUTDOOR ON MODE =\n");
			INPUT_PAYLOAD1(WARM_OUTDOOR_1);
			INPUT_PAYLOAD2(WARM_OUTDOOR_2);
		} else if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
			DPRINT(" = OUTDOOR OFF MODE =\n");
			INPUT_PAYLOAD1(WARM_1);
			INPUT_PAYLOAD2(WARM_2);
		}
		break;

	case mDNIe_VIDEO_COLD_MODE:
		DPRINT(" = VIDEO COLD MODE =\n");
		if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
			DPRINT(" = OUTDOOR ON MODE =\n");
			INPUT_PAYLOAD1(COLD_OUTDOOR_1);
			INPUT_PAYLOAD2(COLD_OUTDOOR_2);
		} else if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
			DPRINT(" = OUTDOOR OFF MODE =\n");
			INPUT_PAYLOAD1(COLD_1);
			INPUT_PAYLOAD2(COLD_2);
		}
		break;

	case mDNIe_CAMERA_MODE:
		DPRINT(" = CAMERA MODE =\n");
		if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
			if (mdnie_tun_state.background == AUTO_MODE) {
				DPRINT(" = AUTO MODE =\n");
				INPUT_PAYLOAD1(AUTO_CAMERA_1);
				INPUT_PAYLOAD2(AUTO_CAMERA_2);
			} else {
				DPRINT(" = STANDARD MODE =\n");
				INPUT_PAYLOAD1(CAMERA_1);
				INPUT_PAYLOAD2(CAMERA_2);
			}
		} else if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
			DPRINT(" = NATURAL MODE =\n");
			INPUT_PAYLOAD1(CAMERA_OUTDOOR_1);
			INPUT_PAYLOAD2(CAMERA_OUTDOOR_2);
		}
		break;

	case mDNIe_NAVI:
		DPRINT(" = NAVI MODE =\n");
		DPRINT("no data for NAVI MODE..\n");
		break;

	case mDNIe_GALLERY:
		DPRINT(" = GALLERY MODE =\n");
		if (mdnie_tun_state.background == STANDARD_MODE) {
			DPRINT(" = STANDARD MODE =\n");
			INPUT_PAYLOAD1(STANDARD_GALLERY_1);
			INPUT_PAYLOAD2(STANDARD_GALLERY_2);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
		} else if (mdnie_tun_state.background == NATURAL_MODE) {
			DPRINT(" = NATURAL MODE =\n");
			INPUT_PAYLOAD1(NATURAL_GALLERY_1);
			INPUT_PAYLOAD2(NATURAL_GALLERY_2);
#endif
		} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
			DPRINT(" = DYNAMIC MODE =\n");
			INPUT_PAYLOAD1(DYNAMIC_GALLERY_1);
			INPUT_PAYLOAD2(DYNAMIC_GALLERY_2);
		} else if (mdnie_tun_state.background == MOVIE_MODE) {
			DPRINT(" = MOVIE MODE =\n");
			INPUT_PAYLOAD1(MOVIE_GALLERY_1);
			INPUT_PAYLOAD2(MOVIE_GALLERY_2);
		} else if (mdnie_tun_state.background == AUTO_MODE) {
			DPRINT(" = AUTO MODE =\n");
			INPUT_PAYLOAD1(AUTO_GALLERY_1);
			INPUT_PAYLOAD2(AUTO_GALLERY_2);
		}
		break;

	case mDNIe_VT_MODE:
		DPRINT(" = VT MODE =\n");
		if (mdnie_tun_state.background == STANDARD_MODE) {
			DPRINT(" = STANDARD MODE =\n");
			INPUT_PAYLOAD1(STANDARD_VT_1);
			INPUT_PAYLOAD2(STANDARD_VT_2);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
		} else if (mdnie_tun_state.background == NATURAL_MODE) {
			DPRINT(" = NATURAL MODE =\n");
			INPUT_PAYLOAD1(NATURAL_VT_1);
			INPUT_PAYLOAD2(NATURAL_VT_2);
#endif
		} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
			DPRINT(" = DYNAMIC MODE =\n");
			INPUT_PAYLOAD1(DYNAMIC_VT_1);
			INPUT_PAYLOAD2(DYNAMIC_VT_2);
		} else if (mdnie_tun_state.background == MOVIE_MODE) {
			DPRINT(" = MOVIE MODE =\n");
			INPUT_PAYLOAD1(MOVIE_VT_1);
			INPUT_PAYLOAD2(MOVIE_VT_2);
		} else if (mdnie_tun_state.background == AUTO_MODE) {
			DPRINT(" = AUTO MODE =\n");
			INPUT_PAYLOAD1(AUTO_VT_1);
			INPUT_PAYLOAD2(AUTO_VT_2);
		}
		break;

#if defined(CONFIG_TDMB)
	case mDNIe_DMB_MODE:
		DPRINT(" = DMB MODE =\n");
		if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
			DPRINT(" = OUTDOOR ON MODE =\n");
			INPUT_PAYLOAD1(OUTDOOR_DMB_1);
			INPUT_PAYLOAD2(OUTDOOR_DMB_2);
		} else if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
			DPRINT(" = OUTDOOR OFF MODE =\n");
			if (mdnie_tun_state.background == STANDARD_MODE) {
				DPRINT(" = STANDARD MODE =\n");
				INPUT_PAYLOAD1(STANDARD_DMB_1);
				INPUT_PAYLOAD2(STANDARD_DMB_2);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
			} else if (mdnie_tun_state.background == NATURAL_MODE) {
				DPRINT(" = NATURAL MODE =\n");
				INPUT_PAYLOAD1(NATURAL_DMB_1);
				INPUT_PAYLOAD2(NATURAL_DMB_2);
#endif
			} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
				DPRINT(" = DYNAMIC MODE =\n");
				INPUT_PAYLOAD1(DYNAMIC_DMB_1);
				INPUT_PAYLOAD2(DYNAMIC_DMB_2);
			} else if (mdnie_tun_state.background == MOVIE_MODE) {
				DPRINT(" = MOVIE MODE =\n");
				INPUT_PAYLOAD1(MOVIE_DMB_1);
				INPUT_PAYLOAD2(MOVIE_DMB_2);
			} else if (mdnie_tun_state.background == AUTO_MODE) {
				DPRINT(" = AUTO MODE =\n");
				INPUT_PAYLOAD1(AUTO_DMB_1);
				INPUT_PAYLOAD2(AUTO_DMB_2);
			}
		}
		break;

	case mDNIe_DMB_WARM_MODE:
		DPRINT(" = DMB WARM MODE =\n");
		if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
			DPRINT(" = OUTDOOR ON MODE =\n");
			INPUT_PAYLOAD1(WARM_OUTDOOR_DMB_1);
			INPUT_PAYLOAD2(WARM_OUTDOOR_DMB_2);
		} else if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
			DPRINT(" = OUTDOOR OFF MODE =\n");
			INPUT_PAYLOAD1(WARM_DMB_1);
			INPUT_PAYLOAD2(WARM_DMB_2);
		}
		break;

	case mDNIe_DMB_COLD_MODE:
		DPRINT(" = DMB COLD MODE =\n");
		if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
			DPRINT(" = OUTDOOR ON MODE =\n");
			INPUT_PAYLOAD1(COLD_OUTDOOR_DMB_1);
			INPUT_PAYLOAD2(COLD_OUTDOOR_DMB_2);
		} else if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
			DPRINT(" = OUTDOOR OFF MODE =\n");
			INPUT_PAYLOAD1(COLD_DMB_1);
			INPUT_PAYLOAD2(COLD_DMB_2);
		}
		break;
#endif

	case mDNIe_BROWSER_MODE:
		DPRINT(" = BROWSER MODE =\n");
		if (mdnie_tun_state.background == STANDARD_MODE) {
			DPRINT(" = STANDARD MODE =\n");
			INPUT_PAYLOAD1(STANDARD_BROWSER_1);
			INPUT_PAYLOAD2(STANDARD_BROWSER_2);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
		} else if (mdnie_tun_state.background == NATURAL_MODE) {
			DPRINT(" = NATURAL MODE =\n");
			INPUT_PAYLOAD1(NATURAL_BROWSER_1);
			INPUT_PAYLOAD2(NATURAL_BROWSER_2);
#endif
		} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
			DPRINT(" = DYNAMIC MODE =\n");
			INPUT_PAYLOAD1(DYNAMIC_BROWSER_1);
			INPUT_PAYLOAD2(DYNAMIC_BROWSER_2);
		} else if (mdnie_tun_state.background == MOVIE_MODE) {
			DPRINT(" = MOVIE MODE =\n");
			INPUT_PAYLOAD1(MOVIE_BROWSER_1);
			INPUT_PAYLOAD2(MOVIE_BROWSER_2);
		} else if (mdnie_tun_state.background == AUTO_MODE) {
			DPRINT(" = AUTO MODE =\n");
			INPUT_PAYLOAD1(AUTO_BROWSER_1);
			INPUT_PAYLOAD2(AUTO_BROWSER_2);
		}
		break;

	case mDNIe_eBOOK_MODE:
		DPRINT(" = eBOOK MODE =\n");
		if (mdnie_tun_state.background == STANDARD_MODE) {
			DPRINT(" = STANDARD MODE =\n");
			INPUT_PAYLOAD1(STANDARD_EBOOK_1);
			INPUT_PAYLOAD2(STANDARD_EBOOK_2);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
		} else if (mdnie_tun_state.background == NATURAL_MODE) {
			DPRINT(" = NATURAL MODE =\n");
			INPUT_PAYLOAD1(NATURAL_EBOOK_1);
			INPUT_PAYLOAD2(NATURAL_EBOOK_2);
#endif
		} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
			DPRINT(" = DYNAMIC MODE =\n");
			INPUT_PAYLOAD1(DYNAMIC_EBOOK_1);
			INPUT_PAYLOAD2(DYNAMIC_EBOOK_2);
		} else if (mdnie_tun_state.background == MOVIE_MODE) {
			DPRINT(" = MOVIE MODE =\n");
			INPUT_PAYLOAD1(MOVIE_EBOOK_1);
			INPUT_PAYLOAD2(MOVIE_EBOOK_2);
		} else if (mdnie_tun_state.background == AUTO_MODE) {
			DPRINT(" = AUTO MODE =\n");
			INPUT_PAYLOAD1(AUTO_EBOOK_1);
			INPUT_PAYLOAD2(AUTO_EBOOK_2);
		}
		break;

#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
	case mDNIe_EMAIL_MODE:
		DPRINT(" = EMAIL MODE =\n");
		if (mdnie_tun_state.background == STANDARD_MODE) {
			DPRINT(" = STANDARD MODE =\n");
			INPUT_PAYLOAD1(AUTO_EMAIL_1);
			INPUT_PAYLOAD2(AUTO_EMAIL_2);
		} else if (mdnie_tun_state.background == NATURAL_MODE) {
			DPRINT(" = NATURAL MODE =\n");
			INPUT_PAYLOAD1(AUTO_EMAIL_1);
			INPUT_PAYLOAD2(AUTO_EMAIL_2);
		} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
			DPRINT(" = DYNAMIC MODE =\n");
			INPUT_PAYLOAD1(AUTO_EMAIL_1);
			INPUT_PAYLOAD2(AUTO_EMAIL_2);
		} else if (mdnie_tun_state.background == MOVIE_MODE) {
			DPRINT(" = MOVIE MODE =\n");
			INPUT_PAYLOAD1(AUTO_EMAIL_1);
			INPUT_PAYLOAD2(AUTO_EMAIL_2);
		} else if (mdnie_tun_state.background == AUTO_MODE) {
			DPRINT(" = AUTO MODE =\n");
			INPUT_PAYLOAD1(AUTO_EMAIL_1);
			INPUT_PAYLOAD2(AUTO_EMAIL_2);
		}
		break;
#endif

	case mDNIE_BLINE_MODE:
		DPRINT(" = BLIND MODE =\n");
		INPUT_PAYLOAD1(COLOR_BLIND_1);
		INPUT_PAYLOAD2(COLOR_BLIND_2);
		break;

	default:
		DPRINT("[%s] no option (%d)\n", __func__, mode);
		return;
	}

#if defined(CONFIG_FB_MSM_MIPI_SAMSUNG_OCTA_CMD_WQHD_PT_PANEL)
	 }else { // 1: fhd

		switch (mode) {
			case mDNIe_UI_MODE:
				DPRINT(" = UI MODE =\n");
				if (mdnie_tun_state.background == STANDARD_MODE) {
					DPRINT(" = STANDARD MODE =\n");
					INPUT_PAYLOAD1(STANDARD_UI_1_FHD);
					INPUT_PAYLOAD2(STANDARD_UI_2_FHD);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
				} else if (mdnie_tun_state.background == NATURAL_MODE) {
					DPRINT(" = NATURAL MODE =\n");
					INPUT_PAYLOAD1(NATURAL_UI_1_FHD);
					INPUT_PAYLOAD2(NATURAL_UI_2_FHD);
#endif
				} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
					DPRINT(" = DYNAMIC MODE =\n");
					INPUT_PAYLOAD1(DYNAMIC_UI_1_FHD);
					INPUT_PAYLOAD2(DYNAMIC_UI_2_FHD);
				} else if (mdnie_tun_state.background == MOVIE_MODE) {
					DPRINT(" = MOVIE MODE =\n");
					INPUT_PAYLOAD1(MOVIE_UI_1_FHD);
					INPUT_PAYLOAD2(MOVIE_UI_2_FHD);
				} else if (mdnie_tun_state.background == AUTO_MODE) {
					DPRINT(" = AUTO MODE =\n");
					INPUT_PAYLOAD1(AUTO_UI_1_FHD);
					INPUT_PAYLOAD2(AUTO_UI_2_FHD);
				}
				break;

			case mDNIe_VIDEO_MODE:
				DPRINT(" = VIDEO MODE =\n");
				if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
					DPRINT(" = OUTDOOR ON MODE =\n");
					INPUT_PAYLOAD1(OUTDOOR_VIDEO_1_FHD);
					INPUT_PAYLOAD2(OUTDOOR_VIDEO_2_FHD);
				} else if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
					DPRINT(" = OUTDOOR OFF MODE =\n");
					if (mdnie_tun_state.background == STANDARD_MODE) {
						DPRINT(" = STANDARD MODE =\n");
						INPUT_PAYLOAD1(STANDARD_VIDEO_1_FHD);
						INPUT_PAYLOAD2(STANDARD_VIDEO_2_FHD);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
					} else if (mdnie_tun_state.background == NATURAL_MODE) {
						DPRINT(" = NATURAL MODE =\n");
						INPUT_PAYLOAD1(NATURAL_VIDEO_1_FHD);
						INPUT_PAYLOAD2(NATURAL_VIDEO_2_FHD);
#endif
					} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
						DPRINT(" = DYNAMIC MODE =\n");
						INPUT_PAYLOAD1(DYNAMIC_VIDEO_1_FHD);
						INPUT_PAYLOAD2(DYNAMIC_VIDEO_2_FHD);
					} else if (mdnie_tun_state.background == MOVIE_MODE) {
						DPRINT(" = MOVIE MODE =\n");
						INPUT_PAYLOAD1(MOVIE_VIDEO_1_FHD);
						INPUT_PAYLOAD2(MOVIE_VIDEO_2_FHD);
					} else if (mdnie_tun_state.background == AUTO_MODE) {
						DPRINT(" = AUTO MODE =\n");
						INPUT_PAYLOAD1(AUTO_VIDEO_1_FHD);
						INPUT_PAYLOAD2(AUTO_VIDEO_2_FHD);
					}
				}
				break;

			case mDNIe_VIDEO_WARM_MODE:
				DPRINT(" = VIDEO WARM MODE =\n");
				if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
					DPRINT(" = OUTDOOR ON MODE =\n");
					INPUT_PAYLOAD1(WARM_OUTDOOR_1_FHD);
					INPUT_PAYLOAD2(WARM_OUTDOOR_2_FHD);
				} else if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
					DPRINT(" = OUTDOOR OFF MODE =\n");
					INPUT_PAYLOAD1(WARM_1_FHD);
					INPUT_PAYLOAD2(WARM_2_FHD);
				}
				break;

			case mDNIe_VIDEO_COLD_MODE:
				DPRINT(" = VIDEO COLD MODE =\n");
				if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
					DPRINT(" = OUTDOOR ON MODE =\n");
					INPUT_PAYLOAD1(COLD_OUTDOOR_1_FHD);
					INPUT_PAYLOAD2(COLD_OUTDOOR_2_FHD);
				} else if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
					DPRINT(" = OUTDOOR OFF MODE =\n");
					INPUT_PAYLOAD1(COLD_1_FHD);
					INPUT_PAYLOAD2(COLD_2_FHD);
				}
				break;

			case mDNIe_CAMERA_MODE:
				DPRINT(" = CAMERA MODE =\n");
				if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
					if (mdnie_tun_state.background == AUTO_MODE) {
						DPRINT(" = AUTO MODE =\n");
						INPUT_PAYLOAD1(AUTO_CAMERA_1_FHD);
						INPUT_PAYLOAD2(AUTO_CAMERA_2_FHD);
					} else {
						DPRINT(" = STANDARD MODE =\n");
						INPUT_PAYLOAD1(CAMERA_1_FHD);
						INPUT_PAYLOAD2(CAMERA_2_FHD);
					}
				} else if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
					DPRINT(" = NATURAL MODE =\n");
					INPUT_PAYLOAD1(CAMERA_OUTDOOR_1_FHD);
					INPUT_PAYLOAD2(CAMERA_OUTDOOR_2_FHD);
				}
				break;

			case mDNIe_NAVI:
				DPRINT(" = NAVI MODE =\n");
				DPRINT("no data for NAVI MODE..\n");
				break;

			case mDNIe_GALLERY:
				DPRINT(" = GALLERY MODE =\n");
				if (mdnie_tun_state.background == STANDARD_MODE) {
					DPRINT(" = STANDARD MODE =\n");
					INPUT_PAYLOAD1(STANDARD_GALLERY_1_FHD);
					INPUT_PAYLOAD2(STANDARD_GALLERY_2_FHD);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
				} else if (mdnie_tun_state.background == NATURAL_MODE) {
					DPRINT(" = NATURAL MODE =\n");
					INPUT_PAYLOAD1(NATURAL_GALLERY_1_FHD);
					INPUT_PAYLOAD2(NATURAL_GALLERY_2_FHD);
#endif
				} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
					DPRINT(" = DYNAMIC MODE =\n");
					INPUT_PAYLOAD1(DYNAMIC_GALLERY_1_FHD);
					INPUT_PAYLOAD2(DYNAMIC_GALLERY_2_FHD);
				} else if (mdnie_tun_state.background == MOVIE_MODE) {
					DPRINT(" = MOVIE MODE =\n");
					INPUT_PAYLOAD1(MOVIE_GALLERY_1_FHD);
					INPUT_PAYLOAD2(MOVIE_GALLERY_2_FHD);
				} else if (mdnie_tun_state.background == AUTO_MODE) {
					DPRINT(" = AUTO MODE =\n");
					INPUT_PAYLOAD1(AUTO_GALLERY_1_FHD);
					INPUT_PAYLOAD2(AUTO_GALLERY_2_FHD);
				}
				break;

			case mDNIe_VT_MODE:
				DPRINT(" = VT MODE =\n");
				if (mdnie_tun_state.background == STANDARD_MODE) {
					DPRINT(" = STANDARD MODE =\n");
					INPUT_PAYLOAD1(STANDARD_VT_1_FHD);
					INPUT_PAYLOAD2(STANDARD_VT_2_FHD);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
				} else if (mdnie_tun_state.background == NATURAL_MODE) {
					DPRINT(" = NATURAL MODE =\n");
					INPUT_PAYLOAD1(NATURAL_VT_1_FHD);
					INPUT_PAYLOAD2(NATURAL_VT_2_FHD);
#endif
				} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
					DPRINT(" = DYNAMIC MODE =\n");
					INPUT_PAYLOAD1(DYNAMIC_VT_1_FHD);
					INPUT_PAYLOAD2(DYNAMIC_VT_2_FHD);
				} else if (mdnie_tun_state.background == MOVIE_MODE) {
					DPRINT(" = MOVIE MODE =\n");
					INPUT_PAYLOAD1(MOVIE_VT_1_FHD);
					INPUT_PAYLOAD2(MOVIE_VT_2_FHD);
				} else if (mdnie_tun_state.background == AUTO_MODE) {
					DPRINT(" = AUTO MODE =\n");
					INPUT_PAYLOAD1(AUTO_VT_1_FHD);
					INPUT_PAYLOAD2(AUTO_VT_2_FHD);
				}
				break;

#if defined(CONFIG_TDMB)
			case mDNIe_DMB_MODE:
				DPRINT(" = DMB MODE =\n");
				if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
					DPRINT(" = OUTDOOR ON MODE =\n");
					INPUT_PAYLOAD1(OUTDOOR_DMB_1);
					INPUT_PAYLOAD2(OUTDOOR_DMB_2);
				} else if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
					DPRINT(" = OUTDOOR OFF MODE =\n");
					if (mdnie_tun_state.background == STANDARD_MODE) {
						DPRINT(" = STANDARD MODE =\n");
						INPUT_PAYLOAD1(STANDARD_DMB_1);
						INPUT_PAYLOAD2(STANDARD_DMB_2);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
					} else if (mdnie_tun_state.background == NATURAL_MODE) {
						DPRINT(" = NATURAL MODE =\n");
						INPUT_PAYLOAD1(NATURAL_DMB_1);
						INPUT_PAYLOAD2(NATURAL_DMB_2);
#endif
					} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
						DPRINT(" = DYNAMIC MODE =\n");
						INPUT_PAYLOAD1(DYNAMIC_DMB_1);
						INPUT_PAYLOAD2(DYNAMIC_DMB_2);
					} else if (mdnie_tun_state.background == MOVIE_MODE) {
						DPRINT(" = MOVIE MODE =\n");
						INPUT_PAYLOAD1(MOVIE_DMB_1);
						INPUT_PAYLOAD2(MOVIE_DMB_2);
					} else if (mdnie_tun_state.background == AUTO_MODE) {
						DPRINT(" = AUTO MODE =\n");
						INPUT_PAYLOAD1(AUTO_DMB_1);
						INPUT_PAYLOAD2(AUTO_DMB_2);
					}
				}
				break;

			case mDNIe_DMB_WARM_MODE:
				DPRINT(" = DMB WARM MODE =\n");
				if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
					DPRINT(" = OUTDOOR ON MODE =\n");
					INPUT_PAYLOAD1(WARM_OUTDOOR_DMB_1);
					INPUT_PAYLOAD2(WARM_OUTDOOR_DMB_2);
				} else if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
					DPRINT(" = OUTDOOR OFF MODE =\n");
					INPUT_PAYLOAD1(WARM_DMB_1);
					INPUT_PAYLOAD2(WARM_DMB_2);
				}
				break;

			case mDNIe_DMB_COLD_MODE:
				DPRINT(" = DMB COLD MODE =\n");
				if (mdnie_tun_state.outdoor == OUTDOOR_ON_MODE) {
					DPRINT(" = OUTDOOR ON MODE =\n");
					INPUT_PAYLOAD1(COLD_OUTDOOR_DMB_1);
					INPUT_PAYLOAD2(COLD_OUTDOOR_DMB_2);
				} else if (mdnie_tun_state.outdoor == OUTDOOR_OFF_MODE) {
					DPRINT(" = OUTDOOR OFF MODE =\n");
					INPUT_PAYLOAD1(COLD_DMB_1);
					INPUT_PAYLOAD2(COLD_DMB_2);
				}
				break;
#endif

			case mDNIe_BROWSER_MODE:
				DPRINT(" = BROWSER MODE =\n");
				if (mdnie_tun_state.background == STANDARD_MODE) {
					DPRINT(" = STANDARD MODE =\n");
					INPUT_PAYLOAD1(STANDARD_BROWSER_1);
					INPUT_PAYLOAD2(STANDARD_BROWSER_2);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
				} else if (mdnie_tun_state.background == NATURAL_MODE) {
					DPRINT(" = NATURAL MODE =\n");
					INPUT_PAYLOAD1(NATURAL_BROWSER_1);
					INPUT_PAYLOAD2(NATURAL_BROWSER_2);
#endif
				} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
					DPRINT(" = DYNAMIC MODE =\n");
					INPUT_PAYLOAD1(DYNAMIC_BROWSER_1);
					INPUT_PAYLOAD2(DYNAMIC_BROWSER_2);
				} else if (mdnie_tun_state.background == MOVIE_MODE) {
					DPRINT(" = MOVIE MODE =\n");
					INPUT_PAYLOAD1(MOVIE_BROWSER_1);
					INPUT_PAYLOAD2(MOVIE_BROWSER_2);
				} else if (mdnie_tun_state.background == AUTO_MODE) {
					DPRINT(" = AUTO MODE =\n");
					INPUT_PAYLOAD1(AUTO_BROWSER_1);
					INPUT_PAYLOAD2(AUTO_BROWSER_2);
				}
				break;

			case mDNIe_eBOOK_MODE:
				DPRINT(" = eBOOK MODE =\n");
				if (mdnie_tun_state.background == STANDARD_MODE) {
					DPRINT(" = STANDARD MODE =\n");
					INPUT_PAYLOAD1(STANDARD_EBOOK_1);
					INPUT_PAYLOAD2(STANDARD_EBOOK_2);
#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
				} else if (mdnie_tun_state.background == NATURAL_MODE) {
					DPRINT(" = NATURAL MODE =\n");
					INPUT_PAYLOAD1(NATURAL_EBOOK_1);
					INPUT_PAYLOAD2(NATURAL_EBOOK_2);
#endif
				} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
					DPRINT(" = DYNAMIC MODE =\n");
					INPUT_PAYLOAD1(DYNAMIC_EBOOK_1);
					INPUT_PAYLOAD2(DYNAMIC_EBOOK_2);
				} else if (mdnie_tun_state.background == MOVIE_MODE) {
					DPRINT(" = MOVIE MODE =\n");
					INPUT_PAYLOAD1(MOVIE_EBOOK_1);
					INPUT_PAYLOAD2(MOVIE_EBOOK_2);
				} else if (mdnie_tun_state.background == AUTO_MODE) {
					DPRINT(" = AUTO MODE =\n");
					INPUT_PAYLOAD1(AUTO_EBOOK_1);
					INPUT_PAYLOAD2(AUTO_EBOOK_2);
				}
				break;

#if !defined(CONFIG_SUPPORT_DISPLAY_OCTA_TFT)
			case mDNIe_EMAIL_MODE:
				DPRINT(" = EMAIL MODE =\n");
				if (mdnie_tun_state.background == STANDARD_MODE) {
					DPRINT(" = STANDARD MODE =\n");
					INPUT_PAYLOAD1(AUTO_EMAIL_1);
					INPUT_PAYLOAD2(AUTO_EMAIL_2);
				} else if (mdnie_tun_state.background == NATURAL_MODE) {
					DPRINT(" = NATURAL MODE =\n");
					INPUT_PAYLOAD1(AUTO_EMAIL_1);
					INPUT_PAYLOAD2(AUTO_EMAIL_2);
				} else if (mdnie_tun_state.background == DYNAMIC_MODE) {
					DPRINT(" = DYNAMIC MODE =\n");
					INPUT_PAYLOAD1(AUTO_EMAIL_1);
					INPUT_PAYLOAD2(AUTO_EMAIL_2);
				} else if (mdnie_tun_state.background == MOVIE_MODE) {
					DPRINT(" = MOVIE MODE =\n");
					INPUT_PAYLOAD1(AUTO_EMAIL_1);
					INPUT_PAYLOAD2(AUTO_EMAIL_2);
				} else if (mdnie_tun_state.background == AUTO_MODE) {
					DPRINT(" = AUTO MODE =\n");
					INPUT_PAYLOAD1(AUTO_EMAIL_1);
					INPUT_PAYLOAD2(AUTO_EMAIL_2);
				}
				break;
#endif

			case mDNIE_BLINE_MODE:
				DPRINT(" = BLIND MODE =\n");
				INPUT_PAYLOAD1(COLOR_BLIND_1);
				INPUT_PAYLOAD2(COLOR_BLIND_2);
				break;

			default:
				DPRINT("[%s] no option (%d)\n", __func__, mode);
				return;
			}
	 }
#endif
	sending_tuning_cmd();
	free_tun_cmd();

	DPRINT("mDNIe_Set_Mode end , mode(%d), background(%d)\n",
		mode, mdnie_tun_state.background);
}
#endif

