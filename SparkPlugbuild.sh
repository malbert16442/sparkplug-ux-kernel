#!/bin/bash

###################################################
##    Copyright (c) 2017, Michael.Albert         ##
##             All rights reserved.              ##
##                                               ##
##        Sparkplug Kernel Build Script          ##
##                                               ##
###################################################

#For Time Calculation
BUILD_START=$(date +"%s")

# Housekeeping
blue='\033[0;34m'
cyan='\033[0;36m'
green='\033[1;32m'
red='\033[0;31m'
nocol='\033[0m'

# Directories
KERNEL_DIR=$PWD
KERN_IMG=$KERNEL_DIR/output/arch/arm/boot/zImage
OUT_DIR=$KERNEL_DIR/zips/klte
COMPILE_LOG=$KERNEL_DIR/logs/compile-$(date +"%Y%m%d").log
SIGNAPK=$KERNEL_DIR/zips/tools/signapk.jar
CERT=$KERNEL_DIR/zips/tools/certificate.pem
KEY=$KERNEL_DIR/zips/tools/key.pk8
SPARKPLUG_VERSION="v1.5"


# Device Spceifics
export ARCH=arm
export CROSS_COMPILE="~/Desktop/toolchain/arm-eabi-4.9/bin/arm-eabi-"
export KBUILD_BUILD_USER="malbert16442"
export KBUILD_BUILD_HOST="sparkplug"

########################
## Start Build Script ##
########################

# Remove Last builds
rm -rf $OUT_DIR/zImage
rm -rf $OUT_DIR/dtb.img

export ARCH=arm
export CROSS_COMPILE=~/Desktop/toolchain/arm-eabi-4.9/bin/arm-eabi-

compile_kernel ()
{
echo -e "$red ********************************************************************************************** $nocol"
echo "                                               "
echo "              Compiling Sparkplug              "
echo "                                               "
echo -e "$red ********************************************************************************************** $nocol"
mkdir output

make -C $(pwd) O=output msm8974_sec_defconfig VARIANT_DEFCONFIG=msm8974pro_sec_klte_eur_defconfig SELINUX_DEFCONFIG=selinux_defconfig

make clean 

make mrproper

export LOCALVERSION=${KERNEL_NAME}-v${KERNEL_VERSION}

make nconfig -C $(pwd) O=output -j14

make -C $(pwd) O=output -j14

cp $KERN_IMG $OUT_DIR
cd $OUT_DIR
zip -r -9 BR_UNSIGNED.zip *
java -jar $SIGNAPK $CERT $KEY BR_UNSIGNED.zip Sparkplug-klte-$SPARKPLUG_VERSION-$(date +"%Y%m%d")-$(date +"%H%M").zip
rm -f BR_UNSIGNED.zip
mv Sparkplug-klte-$SPARKPLUG_VERSION-$(date +"%Y%m%d")-$(date +"%H%M").zip $KERNEL_DIR/Releases
echo -e "$green Kernel Compilation done!"
exit 1

}

compile_kernel | tee $COMPILE_LOG
BUILD_END=$(date +"%s")
DIFF=$(($BUILD_END - $BUILD_START))
echo -e "$blue Build completed in $(($DIFF / 60)) minute(s) and $(($DIFF % 60)) seconds.$nocol"
echo -e "$cyan You could find your Flashable zip in the 'Releases' directory"



