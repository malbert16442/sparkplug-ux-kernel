# AnyKernel2 Ramdisk Mod Script
# osm0sis @ xda-developers

## AnyKernel setup
# EDIFY properties
kernel.string=Sparkplug v1.5
do.devicecheck=1
do.modules=0
do.cleanup=1
do.cleanuponabort=0
device.name1=klte
device.name2=kltevwz
device.name3=kltetmo
device.name4=kltespr
device.name5=kltekdi
device.name6=kltedv

# shell variables
block=/dev/block/platform/msm_sdcc.1/by-name/boot;
is_slot_device=0;
supersu_exclusions=""

## AnyKernel methods (DO NOT CHANGE)
# import patching functions/variables - see for reference
. /tmp/anykernel/tools/ak2-core.sh;
# dump current kernel
dump_boot;

## AnyKernel permissions
# set permissions for included ramdisk files
chmod -R 755 $ramdisk
chmod 644 $ramdisk/sbin/resetprop
chmod 644 $ramdisk/sbin/sysinit.sh

chmod 644 $ramdisk/init.d_support.sh

# ramdisk changes
# end ramdisk changes

write_boot;

## end install

